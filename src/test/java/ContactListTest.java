import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import AddressBook.*;

public class ContactListTest {
    AddressBook addressBook = AddressBook.getAddressBook();

    @Test
    public void addContact() {
        Contact contact = new Contact("contact");
        addressBook.addContact(contact);
        assertEquals("contact", addressBook.getContacts().get(0).getContactFirstName());
        addressBook.deleteContact(0);
    }

    @Test
    public void deleteContact() {
        Contact contact = new Contact("contact");
        addressBook.addContact(contact);
        assertEquals(1, addressBook.getContacts().size());
        for(int i = addressBook.getContacts().size()-1; i>=0;i--){
            addressBook.deleteContact(i);
        }
        assertEquals(0, addressBook.getContacts().size());
    }

    @Test
    public void addContactDetail() {
        Contact contact = new Contact("contact");
        try {
            contact.addContactDetails(new StreetAddress("la Rue"));
            contact.addContactDetails(new PhoneNumber("+41799494008"));
            contact.addContactDetails(new EmailAddress("test@test.ch"));
        } catch (Exception e) {
        }
        addressBook.addContact(contact);
        assertEquals(3, addressBook.getContacts().get(0).getContactDetails().size());
        addressBook.getContacts().remove(contact);
    }

    @Test
    public void loadEmptyContactList(){
        addressBook.loadContacts("src/test/testResourcesRoot/emptyContacts.json");
        assertEquals(0, addressBook.getContacts().size());
    }

    // this test case should not be possible unless the user manually corrupts the json file
    @Test
    public void loadCorruptedContactList(){
        addressBook.loadContacts("src/test/testResourcesRoot/corruptedContacts.json");
        assertEquals(0, addressBook.getContacts().size());
    }


    @Test
    public void saveAndLoadContactsList(){
        int nbOfContacts = 100;
        for(int i = 0; i < nbOfContacts; i++){
            addressBook.addContact(new Contact("contact"));
        }
        addressBook.saveContacts("src/test/testResourcesRoot/contactsTest.json");
        for(int i = nbOfContacts-1; i >= 0; i--){
            addressBook.deleteContact(i);
        }
        addressBook.loadContacts("src/test/testResourcesRoot/contactsTest.json");
        assertEquals("contact", addressBook.getContacts().get(25).getContactFirstName());
        for(int i = nbOfContacts-1; i >= 0; i--){
            addressBook.deleteContact(i);
        }
    }

}
