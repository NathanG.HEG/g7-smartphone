package CalendarFrame;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.calendar.Calendar;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.LocalTime;


class CalendarAPIRequestTest {
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    static Calendar service ;


    @BeforeAll
    static void apiConnect() {
        try {
            service = CalendarAPIRequest.apiConnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        assert true;
    }

    @Test
    void requestNextEvents() {
        CalendarAPIRequest.requestNextEvents(service);
        assert true;
    }

    @Test
    void requestNextEventsList() {
        CalendarAPIRequest.requestNextEventsList(service);
        assert true;

    }

    @Test
    void setEvent() {
        LocalDate dateStart = LocalDate.now().plusDays(1);
        LocalDate dateEnd = LocalDate.now().plusDays(1);
        LocalTime timeStart = LocalTime.now().withNano(0);
        LocalTime timeEnd = LocalTime.now().withNano(0);
        String dateTimeStart = dateStart+" "+timeStart;
        String dateTimeEnd = dateEnd+" "+timeEnd;
        String Title = "JUnit test SetEvent";
        String Location = "IntelliJ";
        String Description = "This is a test Event created by JUnit";
        boolean PopUpReminder = true;
        int popUpTime = 60;
        boolean EmailReminder = true;
        int emailTime = 60*24;
        System.out.println((dateStart));
        CalendarAPIRequest.setEvent(service,Title,Location,Description,dateTimeStart,dateTimeEnd,PopUpReminder,EmailReminder,popUpTime,emailTime);
        assert true ;

    }

    @Test
    void deleteEvent() {
        CalendarAPIRequest.DeleteEvent("123456789",service);
        assert true;

    }
}