import errors.BusinessException;
import errors.ErrorCode;
import gallery.Gallery;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Gallery app unit tests
 */

public class GalleryTestCases {

    private String resourcesPath = "src/test/TestResourcesRoot/";
    private String galleryPath = "src/test/TestResourcesRoot/pictures/";
    private String pictureName = "vancouver_forest.jpg";
    private String newDirectoryPath = "src/test/TestResourcesRoot/newDirectory/";
    private Gallery gallery = loadGallery();

    /**
     * Used to replace the user input when the gallery is first launched.
     * Adds a picture to the gallery.
     *
     * @return the created gallery
     */
    public Gallery loadGallery() {

        //create 'pictures' and 'newDirectory' used later
        File pictures = new File(galleryPath);
        File newDirectory = new File(newDirectoryPath);
        try {
            if (!newDirectory.exists()) {
                newDirectory.mkdir();
            }
            if (!pictures.exists()) {
                pictures.mkdir();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        File galleryDirectoryOnDisk = new File("./galleryDirectory.txt");
        if (galleryDirectoryOnDisk.exists()) {
            try {
                galleryDirectoryOnDisk.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //writes the directory in the txt next to tests location
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(galleryDirectoryOnDisk.getAbsolutePath())))) {
            bw.flush();
            bw.write(galleryPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        gallery = new Gallery();

        try {
            File pictureToAdd = new File(galleryPath + "field.jpg");


            /*
            only adds the picture if it does not exist
            avoid picture already existing exception
            because loadGallery is called for each test
             */
            if (!pictureToAdd.exists()) {
                gallery.addPicture(new File(resourcesPath + "field.jpg"));
            }
        } catch (BusinessException e) {
            e.printStackTrace();
        }

        return gallery;
    }

    @Test
    public void testLoadGallery() {

        //check that the gallery successfully loaded the picture
        assertNotEquals(0, gallery.getPictures().size());
    }


    @Test
    public void testDeletePicture() {

        int gallerySize = gallery.getPictures().size();

        //add a picture in order to delete it later
        File picture = new File(resourcesPath + pictureName);
        try {
            gallery.addPicture(picture);
        } catch (BusinessException e) {
            System.err.println(e.getMessage() + " line 36");
        }

        //check that the picture was successfully added
        gallerySize++;
        assertEquals(gallerySize, gallery.getPictures().size());


        //try to delete the picture
        try {
            gallery.deletePicture(new File(galleryPath + pictureName));
        } catch (BusinessException e) {
            System.err.println(e.getMessage());
        }

        //check that the picture was successfully deleted
        gallerySize--;
        assertEquals(gallerySize, gallery.getPictures().size());

    }


    @Test
    public void testRenamePicture() {

        int gallerySize = gallery.getPictures().size();
        String aquatisPictureName = "aquatis_hotel.png";
        String zooPictureName = "plan_zoo.jpg";

        File aquatisPicture = new File(resourcesPath + aquatisPictureName);
        File zooPicture = new File(resourcesPath + zooPictureName);


        //add two pictures to test the rename function
        try {
            gallery.addPicture(aquatisPicture);
            gallery.addPicture(zooPicture);
        } catch (BusinessException e) {
            System.err.println(e.getMessage());
        }

        //check that the pictures were successfully added
        gallerySize += 2;
        assertEquals(gallerySize, gallery.getPictures().size());

        /*
        rename one of the picture
        shouldn't throw any exception
         */
        File aquatisPictureInGallery = new File(galleryPath + aquatisPictureName);
        try {
            aquatisPictureName = "aquatis.png";
            gallery.renameFile(aquatisPictureInGallery, aquatisPictureName);
        } catch (BusinessException e) {
            System.err.println(e.getMessage());
        }

        //check that the picture was successfully renamed
        File aquatisPictureRenamed = gallery.getPicture(aquatisPictureName);
        assertEquals(aquatisPictureName, aquatisPictureRenamed.getName());

        /*
        rename exceptions
         */

        //those exceptions are used to check the error codes later
        BusinessException emptyName = new BusinessException("", ErrorCode.EMPTY_NAME);
        BusinessException sameName = new BusinessException("", ErrorCode.SIMILAR_NEW_NAME);
        BusinessException existingPicture = new BusinessException("", ErrorCode.ALREADY_EXISTING_FILE);
        BusinessException invalidFormat = new BusinessException("", ErrorCode.UNSUPPORTED_FORMAT);

        //empty new name
        try {
            gallery.renameFile(zooPicture, "");
        } catch (BusinessException e) {
            assertEquals(emptyName.getErrorCode(), e.getErrorCode());
        }
        //check that the picture wasn't renamed
        assertEquals(zooPictureName, zooPicture.getName());


        //new name similar as the current one
        try {
            gallery.renameFile(zooPicture, zooPictureName);
        } catch (BusinessException e) {
            assertEquals(sameName.getErrorCode(), e.getErrorCode());
        }
        //check that the picture wasn't renamed
        assertEquals(zooPictureName, zooPicture.getName());


        //two pictures with the same name in the gallery
        try {
            gallery.renameFile(zooPicture, aquatisPictureName);
        } catch (BusinessException e) {
            assertEquals(existingPicture.getErrorCode(), e.getErrorCode());
        }
        //check that the picture wasn't renamed
        assertEquals(zooPictureName, zooPicture.getName());


        //unsupported extension
        try {
            gallery.renameFile(zooPicture, "zoo");
        } catch (BusinessException e) {
            assertEquals(invalidFormat.getErrorCode(), e.getErrorCode());
        }
        //check that the picture wasn't renamed
        assertEquals(zooPictureName, zooPicture.getName());

        /*
        delete the pictures from the gallery
        to avoid ALREADY_EXISTING_FILE error
        when launching this test case several times
         */
        try {
            gallery.deletePicture(new File(galleryPath + aquatisPictureName));
            gallery.deletePicture(new File(galleryPath + zooPictureName));
        } catch (BusinessException e) {
            System.err.println(e.getMessage());
        }
    }


    @Test
    public void testChangeDirectory() {

        //writes the new directory on disk
        gallery.changeDirectory(newDirectoryPath);

        //create the new gallery which reads the path in the txt file
        Gallery newGallery = new Gallery();

        //check that the new gallery used the path written in txt file
        assertEquals(newDirectoryPath, newGallery.getDirectoryPath());

        /*
        reset the gallery path to avoid problems when
        launching this test case several times
         */
        newGallery.changeDirectory(galleryPath);

    }


    @Test
    public void testAddPicture() {

        File txt = new File(resourcesPath + "galleryTest.txt");
        File picture = new File(resourcesPath + pictureName);
        int gallerySize = gallery.getPictures().size();


        /*
        add a txt file to the gallery
        should throw UNSUPPORTED FORMAT error
         */
        try {
            gallery.addPicture(txt);
        } catch (BusinessException e) {
            assertEquals(ErrorCode.UNSUPPORTED_FORMAT, e.getErrorCode());
        }
        //ensure that the txt file wasn't added to the gallery
        assertEquals(gallerySize, gallery.getPictures().size());


        /*
        add a picture to the gallery
        shouldn't throw any exception
         */
        gallerySize = gallery.getPictures().size();

        try {
            gallery.addPicture(picture);
        } catch (BusinessException e) {
            e.printStackTrace();
        }

        //check that the picture was successfully added
        assertEquals(gallerySize + 1, gallery.getPictures().size());

        /*
        add the vancouver picture again
        should throw a ALREADY_EXISTING_FILE error
         */
        gallerySize = gallery.getPictures().size();

        try {
            gallery.addPicture(picture);
        } catch (BusinessException e) {
            assertEquals(ErrorCode.ALREADY_EXISTING_FILE, e.getErrorCode());
        }

        //check that the picture was not added
        assertEquals(gallerySize, gallery.getPictures().size());


        /*
        delete the picture from the gallery
        to avoid ALREADY_EXISTING_FILE error
        when launching this test case several times
         */
        try {
            //picture in the directory
            File newPicture = new File(galleryPath + pictureName);

            gallery.deletePicture(newPicture);
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }


}
