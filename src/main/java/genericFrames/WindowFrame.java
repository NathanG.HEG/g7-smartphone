package genericFrames;

import javax.swing.*;
import java.awt.*;

/**
 * @author Nathan Gaillard, nathan.gaillard@students.hevs.ch
 * @since 14/06/2021
 * <p>
 * WindowFrame is a JFrame which has smartphone dimensions.
 * The LauncherPanel and the ApplicationPanels are opened in this Frame.
 */

public class WindowFrame extends JFrame {
    final static int windowFrameWidth = 405;
    final static int windowFrameHeight = 720;
    final static Dimension windowFrameDimension = new Dimension(windowFrameWidth, windowFrameHeight);
    static LauncherPanel launcherPanel;

    public WindowFrame() {
        setSize(windowFrameDimension);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        launcherPanel = new LauncherPanel(this);
        add(launcherPanel);
        setVisible(true);
    }

    public static Dimension getWindowFrameDimension() {
        return windowFrameDimension;
    }

    public static int getWindowFrameWidth() {
        return windowFrameWidth;
    }

    public static int getWindowFrameHeight() {
        return windowFrameHeight;
    }
}
