package genericFrames;

import javax.swing.*;
import java.awt.*;

/**
 * @author Nathan Gaillard, nathan.gaillard@students.hevs.ch
 * @since 14/06/2021
 * <p>
 * ApplicationPanel is a JPanel which is used by the application panels.
 */

public class ApplicationPanel extends JPanel {

    public ApplicationPanel() {
        setVisible(false);
    }
}
