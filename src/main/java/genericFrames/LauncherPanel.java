package genericFrames;

import AddressBook.AddressBook;
import AddressBook.AddressBookFrames.AddressBookPanel;
import CalendarFrame.*;
import gallery.GalleryPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Nathan Gaillard, nathan.gaillard@students.hevs.ch
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 14/06/2021
 * <p>
 * LauncherPanel is a JPanel from which apps are created.
 * LauncherPanel interface simulates a smartphone launcher.
 */
public class LauncherPanel extends JPanel {

    final static int XBOUND = 0;
    final static int YBOUND = 25;
    final static int WIDTHBOUND = WindowFrame.windowFrameWidth;
    final static int HEIGHTBOUND = WindowFrame.windowFrameHeight - 115;
    static final int NBOFAPPS = 3;
    public ApplicationPanel[] apps = new ApplicationPanel[NBOFAPPS];
    int currentApp = -1;
    JButton[] appButtons = new JButton[NBOFAPPS];
    JFrame windowFrame;
    LauncherPanel launcherPanel;

    /**
     * Creates a new LauncherPanel in a windowFrame.
     *
     * @param windowFrame The windowFrame that called LauncherPanel.
     */
    public LauncherPanel(JFrame windowFrame) {
        setLayout(null);
        setBackground(Color.darkGray);
        showTime();
        this.windowFrame = windowFrame;
        launcherPanel = this;

        appButtons[0] = new JButton();
        appButtons[0].setActionCommand("0");
        appButtons[0].setBounds(WIDTHBOUND / 2 - 75 / 2, HEIGHTBOUND / 2 - 100, 75, 75);
        appButtons[0].addActionListener(new MyActionListener());
        appButtons[0].setIcon(loadImageIcon("galleryAppIcon.png"));
        add(appButtons[0]);

        appButtons[1] = new JButton();
        appButtons[1].setActionCommand("1");
        appButtons[1].setBounds(WIDTHBOUND / 2 - 75 / 2, HEIGHTBOUND / 2, 75, 75);
        appButtons[1].addActionListener(new MyActionListener());
        appButtons[1].setIcon(loadImageIcon("calendar.png"));
        add(appButtons[1]);

        appButtons[2] = new JButton();
        appButtons[2].setActionCommand("2");
        appButtons[2].setBounds(WIDTHBOUND / 2 - 75 / 2, HEIGHTBOUND / 2 + 100, 75, 75);
        appButtons[2].addActionListener(new MyActionListener());
        appButtons[2].setIcon(loadImageIcon("contacts.png"));
        add(appButtons[2]);

        JButton backToLauncher = new JButton();
        backToLauncher.setActionCommand("home");
        backToLauncher.addActionListener(new MyActionListener());
        backToLauncher.setBounds(WIDTHBOUND / 2 - 150 / 2, HEIGHTBOUND + 35, 150, 30);
        backToLauncher.setIcon(loadImageIcon("home_button.png"));
        add(backToLauncher);

        JLabel batteryIconLabel = new JLabel();
        batteryIconLabel.setBounds(20, 3, 40, 20);
        ImageIcon batteryIcon = loadImageIcon("batteryIcon.png");
        batteryIconLabel.setIcon(batteryIcon);
        add(batteryIconLabel);
    }

    public static int getXBOUND() {
        return XBOUND;
    }

    public static int getYBOUND() {
        return YBOUND;
    }

    public static int getWIDTHBOUND() {
        return WIDTHBOUND;
    }

    public static int getHEIGHTBOUND() {
        return HEIGHTBOUND;
    }

    /**
     * Adds the time at when the application was launched.
     */
    private void showTime() {
        final long msInADay = 86400000;
        long h, m;
        JLabel timeLabel = new JLabel();

        h = System.currentTimeMillis() % msInADay / 1000 / 60 / 60 + 2;
        m = System.currentTimeMillis() % msInADay / 1000 / 60 - (h - 2) * 60;

        String hour = String.valueOf(h);
        String minute = String.valueOf(m);

        if(h < 10){
            hour = "0" + h;
        }
        if (m < 10){
            minute = "0" + m;
        }

        timeLabel.setText(hour + ":" + minute);

        timeLabel.setForeground(Color.WHITE);
        JPanel timePanel = new JPanel();
        timePanel.add(timeLabel);
        timePanel.setBackground(getBackground());
        timePanel.setBounds(WindowFrame.windowFrameWidth - 80, 1, 60, 20);
        add(timePanel);
    }

    /**
     * Loads apps icons from resources folder.
     *
     * @param resourceName of the picture
     * @return an ImageIcon of the desired resource.
     */
    public ImageIcon loadImageIcon(String resourceName) {

        ImageIcon imageIcon = null;

        try {
            imageIcon = new ImageIcon(ImageIO.read(getClass().getClassLoader().getResource(resourceName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageIcon;
    }

    /**
     * Set visible or invisible the apps buttons.
     *
     * @param value true=visible, false=invisible
     */
    private void buttonsSetVisible(boolean value) {
        for (JButton button : appButtons) {
            if (button != null) {
                button.setVisible(value);
            }
        }
    }

    /**
     * Called when a new directory is selected.
     * Recreates a GalleryPanel with the new gallery.
     */
    public void reloadGallery() {
        apps[0].setVisible(false);
        apps[0] = new GalleryPanel(launcherPanel, windowFrame);
        apps[0].setBounds(XBOUND, YBOUND, WIDTHBOUND, HEIGHTBOUND);
        add(apps[0]);
        apps[0].setVisible(true);
    }

    private class MyActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "0":
                    currentApp = 0;
                    WindowFrame.launcherPanel.setEnabled(false);
                    apps[0] = new GalleryPanel(launcherPanel, windowFrame);
                    apps[0].setBounds(XBOUND, YBOUND, WIDTHBOUND, HEIGHTBOUND);
                    add(apps[0]);
                    apps[0].setVisible(true);
                    buttonsSetVisible(false);
                    break;
                case "1":
                    currentApp = 1;
                    WindowFrame.launcherPanel.setEnabled(false);
                    apps[1] = new CalendarPanel();
                    apps[1].setBounds(XBOUND, YBOUND, WIDTHBOUND, HEIGHTBOUND);
                    add(apps[1]);
                    apps[1].setVisible(true);
                    buttonsSetVisible(false);
                    break;
                case "2":
                    currentApp = 2;
                    WindowFrame.launcherPanel.setEnabled(false);
                    apps[2] = new AddressBookPanel();
                    apps[2].setBounds(XBOUND, YBOUND, WIDTHBOUND, HEIGHTBOUND);
                    add(apps[2]);
                    apps[2].setVisible(true);
                    buttonsSetVisible(false);
                    break;

                case "home":
                    System.out.println(currentApp);
                    if (currentApp == 2) {
                        AddressBook ab = AddressBook.getAddressBook();
                        ab.saveContactsInJarEnvir();
                    }
                    if (currentApp != -1) {
                        apps[currentApp].setVisible(false);
                        buttonsSetVisible(true);
                        currentApp = -1;
                    }

                    break;
            }
        }
    }


}