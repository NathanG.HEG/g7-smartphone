package CalendarFrame;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;
import com.google.api.services.calendar.Calendar;
import genericFrames.ApplicationPanel;
import genericFrames.LauncherPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Fabio Maio de Almeida
 * @since 2021-06-14
 */


public class AddEventPanel extends ApplicationPanel {
    JPanel firstPanel = new JPanel();
    JPanel secPanel = new JPanel();
    public DatePicker dateStart = new DatePicker();
    public DatePicker dateEnd = new DatePicker();
    JTextField Titre = new JTextField();
    JTextField Description = new JTextField();
    JTextField Location = new JTextField();
    JButton back = new JButton("Go Back");
    public TimePicker heureDebut = new TimePicker();
    public TimePicker heureFin = new TimePicker();
    JCheckBox reminderEmail = new JCheckBox("E-mail", false);
    JCheckBox reminderPopUp = new JCheckBox("Pop-Up", false);
    JSpinner timeEmail = new JSpinner();
    JSpinner timePopUp = new JSpinner();
    JLabel StartDate = new JLabel("Date de début");
    JLabel EndDate = new JLabel("Date de fin");
    JButton CreateEvent = new JButton("Create Event");
    Calendar service;
    JLabel LabelTitre = new JLabel("Titre");
    JLabel LabelDescription = new JLabel("Description");
    JLabel LabelLieu = new JLabel("Lieu");
    JLabel LabelHDebut = new JLabel("Heure début");
    JLabel LabelHFin = new JLabel("Heure fin");
    CalendarPanel calendarFrame;

    /**
     * Constructor
     * @param calendarPanel parent panel
     */
    public AddEventPanel(CalendarPanel calendarPanel) {
        this.calendarFrame = calendarPanel;
        setBackground(Color.lightGray);
        setVisible(true);
        setLayout(null);
        firstPanel.setLayout(new GridLayout(0, 2));
        firstPanel.setBounds(5,0, LauncherPanel.getWIDTHBOUND()-25,LauncherPanel.getHEIGHTBOUND()/2);
        firstPanel.add(LabelTitre);
        firstPanel.add(Titre);
        firstPanel.add(LabelDescription);
        firstPanel.add(Description);
        firstPanel.add(LabelLieu);
        firstPanel.add(Location);
        firstPanel.add(StartDate);
        firstPanel.add(dateStart);
        firstPanel.add(EndDate);
        firstPanel.add(dateEnd);
        firstPanel.add(LabelHDebut);
        firstPanel.add(heureDebut);
        firstPanel.add(LabelHFin);
        firstPanel.add(heureFin);
        firstPanel.setBackground(Color.lightGray);
        add(firstPanel);
        secPanel.add(reminderPopUp);
        secPanel.add(timePopUp);
        secPanel.add(reminderEmail);
        secPanel.add(timeEmail);
        CreateEvent.setActionCommand("0");
        CreateEvent.addActionListener(new MyActionListenerEvent());
        secPanel.add(CreateEvent);
        secPanel.setBounds(0,firstPanel.getHeight(),LauncherPanel.getWIDTHBOUND(),LauncherPanel.getHEIGHTBOUND()/4);
        secPanel.setBackground(Color.lightGray);
        add(secPanel);
        back.addActionListener(new MyActionListenerEvent());
        back.setActionCommand("back");
        back.setBounds(LauncherPanel.getWIDTHBOUND()/2-75,450,150,30);
        add(back);


    }

    public class MyActionListenerEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "0":
                    String debut = dateStart.getDate() + " " + heureDebut.getTime() + ":00";
                    String fin = dateEnd.getDate() + " " + heureFin.getTime() + ":00";
                    try {
                        CalendarAPIRequest.setEvent(service = CalendarAPIRequest.apiConnect(), Titre.getText(),
                                Location.getText(), Description.getText(), debut, fin, reminderEmail.isSelected(), reminderPopUp.isSelected(),
                                (int) timeEmail.getValue(), (int) timePopUp.getValue());
                        JFrame result = new JFrame();
                        JOptionPane.showMessageDialog(result, "Événement créé");
                        back.doClick();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                break;
                case "back":
                    setVisible(false);
                    calendarFrame.mainPanel.setVisible(true);
                    calendarFrame.button[0].doClick();
            }

        }
    }
}

