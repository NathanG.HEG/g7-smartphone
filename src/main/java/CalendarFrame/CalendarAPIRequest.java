package CalendarFrame;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.*;

import java.io.*;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

/**
 * CalendarAPIRequest contains all the operations to create an authorized Credential, create a link to the API,
 * request the next events, create an event and delete an event.
 * @author Fabio Maio de Almeida
 * @since 2021-06-14
 */

public class CalendarAPIRequest {
    private static final String APPLICATION_NAME = "Calendar G7-Smartphone";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR);
    private static final String CREDENTIALS_FILE_PATH = "credentials.json";
    private static final String CalendarID = "primary";
    private static final String UserID = "api-762@calendar-313309.iam.gserviceaccount.com";


    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        GoogleClientSecrets clientSecrets;
        try (InputStream in = CalendarAPIRequest.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH)) {
            {
            }
            ;

            if (in == null) {
                throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
            }
            clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
        }

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize(UserID);
    }

    /**
     * Initialize the Google calendar
     * @return
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public static Calendar apiConnect() throws IOException, GeneralSecurityException {

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        return service;
    }

    /**
     * Requests next events in google calendar
     * @param service
     * @return a String array for JList
     */
    public static String[] requestNextEvents(Calendar service) {
        String[] output = new String[13];
        DateTime now = new DateTime(System.currentTimeMillis());
        Events events = null;
        try {
            events = service.events().list(CalendarID)
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Event> items = events.getItems();
        if (items.isEmpty()) {
            //System.out.println("Aucun évenement trouvé.");
        } else {

            int i = 0;
            for (Event event : items) {
                i++;
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    start = event.getStart().getDate();
                }

                output[i] = (event.getSummary() + " " + start);


            }
        }
        return output;
    }

    /**
     * Requests next events in google calendar
     * @param service
     * @return a List<Event> for the delete operation
     */
    public static List<Event> requestNextEventsList(Calendar service) {
        String[] output = new String[10];
        DateTime now = new DateTime(System.currentTimeMillis());
        Events events = null;
        try {
            events = service.events().list(CalendarID)
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Event> items = events.getItems();
        return items;

    }

    /**
     * Create an event in the google calendar
     * @param service = the calendar
     * @param titleEvent string containing the title of the event
     * @param locationEvent string containing the location of the event
     * @param descriptionEvent string containing the description of the event
     * @param startDate string containing the starting date of the event
     * @param endDate string containing the ending date of the event
     * @param reminderEmail boolean - true if want to receive an e-mail as reminder
     * @param reminderPopUp boolean - true if want to receive a pop-up as reminder
     * @param reminderTimeEmail int containing how long before in minutes you want to be reminded by e-mail
     * @param reminderTimePopUp int containing how long before in minutes you want to be reminded by pop-up
     */
    public static void setEvent(Calendar service, String titleEvent, String locationEvent, String descriptionEvent,
                                String startDate, String endDate, boolean reminderEmail, boolean reminderPopUp,
                                int reminderTimeEmail, int reminderTimePopUp) {


        //Event DATA
        Event event = new Event();
        event.setSummary(titleEvent);
        event.setLocation(locationEvent);
        event.setDescription(descriptionEvent);
        if(titleEvent== "JUnit test SetEvent")
            event.setId("123456789");


        //Event Time
        //startDate-endDate format : YYYY-MM-DD
        //starDateTime-endTime : YYYY-MM-DD HH:MM:SS:MMMM
        Timestamp sDate = Timestamp.valueOf(startDate);
        Timestamp eDate = Timestamp.valueOf(endDate);
        DateTime start = new DateTime(sDate, TimeZone.getTimeZone("UTC+2"));
        event.setStart(new EventDateTime().setDateTime(start));
        DateTime end = new DateTime(eDate, TimeZone.getTimeZone("UTC+2"));
        event.setEnd(new EventDateTime().setDateTime(end));


        //reminder
        if (reminderEmail == true | reminderPopUp == true) {
            if (reminderEmail == true & reminderPopUp == true) {
                EventReminder[] reminderOverrides = new EventReminder[]{
                        new EventReminder().setMethod("email").setMinutes(reminderTimeEmail),
                        new EventReminder().setMethod("popup").setMinutes(reminderTimePopUp),
                };
                Event.Reminders reminders = new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(reminderOverrides));
                event.setReminders(reminders);
            }
            if (reminderEmail == true & reminderPopUp == false) {
                EventReminder[] reminderOverrides = new EventReminder[]{
                        new EventReminder().setMethod("email").setMinutes(reminderTimeEmail),
                };
                Event.Reminders reminders = new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(reminderOverrides));
                event.setReminders(reminders);
            }
            if (reminderEmail == false & reminderPopUp == true) {
                EventReminder[] reminderOverrides = new EventReminder[]{
                        new EventReminder().setMethod("email").setMinutes(reminderTimeEmail),
                        new EventReminder().setMethod("popup").setMinutes(reminderTimePopUp),
                };
                Event.Reminders reminders = new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(reminderOverrides));
                event.setReminders(reminders);
            }


        }


        //Event creation
        try {
            service.events().insert(CalendarID, event).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete an Event
     * @param IdEvent String containing the id of the event you to delete
     * @param service = the calendar
     */
    static void DeleteEvent(String IdEvent, Calendar service) {
        try {
            service.events().delete(CalendarID, IdEvent).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a birthday event for each person in AdresseBook
     * @param service = the calendar
     * @param name string containing the name of the contact
     * @param date string containing the birthday date of the contact
     */
    public static void birthdayEventCreation(Calendar service, String name, String date) {
        //AAAA-MM-JJTHH:MM:SS:MMMM
        String newDate = LocalDateTime.now().getYear()+"-"+date;

        Event event = new Event()
                .setSummary("Anniveraire de "+name)
                .setLocation("Chez "+name);



        String startDate = newDate; //rajouter date de naissance
        EventDateTime start = new EventDateTime()
                .setDate(DateTime.parseRfc3339(startDate))
                .setTimeZone("UTC+2");
        event.setStart(start);

        String endDate = newDate; //same
        EventDateTime end = new EventDateTime()
                .setDate(DateTime.parseRfc3339(endDate))
                .setTimeZone("UTC+2");
        event.setEnd(end);

        String[] recurrence = new String[] {"RRULE:FREQ=YEARLY"};
        event.setRecurrence(Arrays.asList(recurrence));

        EventReminder[] reminderOverrides = new EventReminder[] {
                new EventReminder().setMethod("email").setMinutes(24 * 60),
                new EventReminder().setMethod("popup").setMinutes(24*60),
        };
        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);

        try {
            event = service.events().insert(CalendarID, event).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
