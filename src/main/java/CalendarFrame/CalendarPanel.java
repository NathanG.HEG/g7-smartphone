package CalendarFrame;


import AddressBook.AddressBook;
import AddressBook.Birthday;
import AddressBook.Contact;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import genericFrames.ApplicationPanel;
import genericFrames.LauncherPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.ArrayList;

import static CalendarFrame.CalendarAPIRequest.*;

/**
 * CalendarPanel is the main panel of the calendar. It contains the visual calendar,
 * the list of the next events and buttons to add (addEventFrame) and delete events.
 * @author Fabio Maio de Almeida
 * @since 2021-06-14
 */



public class CalendarPanel extends ApplicationPanel {


    JList<String> eventsList = new JList<String>();

    public JButton[] button = new JButton[3];
    public com.github.lgooddatepicker.components.CalendarPanel calendarSubPanel = new com.github.lgooddatepicker.components.CalendarPanel();
    public Calendar service;
    JFrame delete = new JFrame();
    JOptionPane optionPane = new JOptionPane();
    CalendarPanel calendarPanel;
    JPanel mainPanel = new JPanel();

    /**
     * Constructor
     */
    public CalendarPanel() {
        calendarPanel = this;

        try {
            service = apiConnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        addBirthdayToCalendar(AddressBook.getAddressBook());
        mainPanel.setLayout(null);
        setLayout(new BorderLayout());

        setBackground(Color.lightGray);
        button[0] = new JButton("Update next events");
        button[0].setBounds(25, 25, 225, 35);
        button[0].setActionCommand("0");
        button[0].addActionListener(new MyActionListenerCalendar());
        mainPanel.add(button[0]);

        button[1] = new JButton("+");
        button[1].setBounds(300, 25, 45, 35);
        button[1].setActionCommand("1");
        button[1].addActionListener(new MyActionListenerCalendar());
        mainPanel.add(button[1]);

        eventsList.setBounds(50,365, LauncherPanel.getWIDTHBOUND()-125, 180);
        eventsList.setBackground(Color.WHITE);

        eventsList.setLayout(new BorderLayout());
        eventsList.setAlignmentX(getAlignmentX());

        calendarSubPanel.setSelectedDate(LocalDate.now());
        calendarSubPanel.setBounds(75, 87, LauncherPanel.getWIDTHBOUND()-50, 250);
        mainPanel.add(calendarSubPanel);
        mainPanel.add(eventsList);

        button[0].doClick();

        button[2] = new JButton("Supprimer");
        button[2].setBounds(50, 555, LauncherPanel.getWIDTHBOUND()-125, 35);
        button[2].setActionCommand("2");
        button[2].addActionListener(new MyActionListenerCalendar());
        mainPanel.add(button[2]);
        mainPanel.setVisible(true);
        add(mainPanel, BorderLayout.CENTER);


    }

    /**
     * addBirthdayToCalendar gets the birthday date of each contact that have one and
     * adds them if they are not in the calendar.
     * @param ab AdresseBook to get contacts
     */
    private void addBirthdayToCalendar(AddressBook ab) {
        ab.loadContacts(ab.getJarPath());
        ArrayList<Contact> c = ab.getContacts();
        for(int i = 0; i<c.size();i++){
            Birthday temp = c.get(i).getBirthday();
            String date;
            if(!c.get(i).isBirthdayAddedToCalendar()){
                if(temp != null) {
                    String day=String.valueOf(temp.getDay());
                    String month=String.valueOf(temp.getMonth());
                    if(temp.getDay()<10)
                        day="0"+temp.getDay();
                    if(temp.getMonth()<10)
                        month="0"+(temp.getMonth());

                    date = month+"-"+day;
                    birthdayEventCreation(service, c.get(i).getContactFirstName() + " " + c.get(i).getContactLastName(), date);
                    c.get(i).setBirthdayAddedToCalendar(true);

                }
            }
        }
    ab.saveContactsInJarEnvir();
    }



    class MyActionListenerCalendar implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "0":
                    int i = 0;
                    String[] request;
                    String[] finalRequest;
                    int length = 0;
                    request = (CalendarAPIRequest.requestNextEvents(service));
                    for (int j = 1; j < request.length; j++) {
                        if (request[j] != null)
                            length++;
                    }
                    finalRequest = new String[length];
                    while (request[i + 1] != null) {
                        finalRequest[i] = request[i + 1];
                        i++;
                    }
                    // Create JList
                    eventsList.setVisible(true);
                    eventsList.setListData(finalRequest);

                    break;
                case "1":
                    AddEventPanel addEventPanel = new AddEventPanel(calendarPanel);
                    mainPanel.setVisible(false);
                    addEventPanel.setVisible(true);
                    addEventPanel.dateStart.setDate(calendarSubPanel.getSelectedDate());
                    addEventPanel.dateEnd.setDate(calendarSubPanel.getSelectedDate());
                    addEventPanel.heureDebut.setTimeToNow();
                    addEventPanel.heureFin.setTimeToNow();
                    add(addEventPanel);
                    break;
                case "2":
                    if (eventsList.getSelectedIndex() == -1) {
                        JOptionPane erreur = new JOptionPane();
                        JFrame err = new JFrame();
                        JOptionPane.showMessageDialog(err, "Veuillez sélectionner l'évènement que vous souhaitez supprimer !", "Erreur", JOptionPane.WARNING_MESSAGE);
                    } else {
                        Object[] options = {"Annuler",
                                "Supprimer"};

                        int output = optionPane.showOptionDialog(delete,
                                "Voulez vous supprimez cet évènement ?",
                                "Suppression évènement",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                options,
                                options[1]);
                        if (output == JOptionPane.NO_OPTION) {
                            java.util.List<Event> items = CalendarAPIRequest.requestNextEventsList(service);
                            Event event = items.get(eventsList.getSelectedIndex());
                            String Id = event.getId();
                            DeleteEvent(Id, service);
                            button[0].doClick();

                        }
                    }
                    break;


            }


        }
    }
}

