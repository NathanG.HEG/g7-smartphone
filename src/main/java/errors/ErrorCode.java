package errors;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 *
 * <p>
 * Enum of ErrorCodes used in BusinessException
 * 100 range gallery
 * 200 range contact
 * 300 range agenda
 */

public enum ErrorCode {

    /**
     * Gallery
     */
    UNSUPPORTED_FORMAT(100),
    ALREADY_EXISTING_FILE(101),
    EMPTY_NAME(110),
    NON_EXISTING_DIRECTORY(111),
    NOT_A_DIRECTORY(112),
    SIMILAR_NEW_NAME(113),
    TECHNICAL_ERROR(120),
    IOEXCEPTION(121),
    INVALID_DIRECTORY(122),

    /**
     * Contact
     */
    INVALID_DATE(201),
    INVALID_EMAIL(202),
    INVALID_PHONENUMBER(203),
    INVALID_ADDRESS(204);


    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

