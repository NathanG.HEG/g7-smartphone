package errors;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 *
 * BusinessException is the generic exception used for the agenda, contact and gallery apps.
 * ErrorCodes are used to identify the exception's source.
 */
public class BusinessException extends Exception {

    ErrorCode errorCode;

    public BusinessException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCodeInt() {
        return errorCode.getCode();
    }

    public ErrorCode getErrorCode(){
        return errorCode;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
