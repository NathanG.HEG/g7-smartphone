package gallery;

import errors.BusinessException;
import genericFrames.ApplicationPanel;
import genericFrames.LauncherPanel;
import genericFrames.WrapLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 * <p>
 * GalleryPanel is a JPanel which displays all the pictures.
 * From this panel, the user can add a picture, enter the edition screen or set a new Directory.
 */

public class GalleryPanel extends ApplicationPanel {

    //smartphone app dimension
    private final int HEIGHTBOUND = LauncherPanel.getHEIGHTBOUND();
    private final int WIDTHBOUND = LauncherPanel.getWIDTHBOUND();

    //displayed picture height and width
    private final int PICTUREDIMENSION = 115;
    private final LauncherPanel launcherPanel;
    private final JFrame windowFrame;
    private final GalleryPanel galleryPanel;
    private final Gallery gallery;
    EditionPanel editionPanel;
    JPanel mainPanel;
    JPanel setDirectoryPanel;

    //graphic components
    private ArrayList<ImageIcon> picturesImageIcons;
    private ArrayList<JLabel> jLabels;
    private JPanel picturesPanel;

    /**
     * Constructor
     *
     * @param launcherPanel superPanel which create the GalleryPanel
     * @param windowFrame   superFrame, mandatory to open the File explorer
     */
    public GalleryPanel(LauncherPanel launcherPanel, JFrame windowFrame) {

        //initialises the superFrames and current frame
        this.launcherPanel = launcherPanel;
        this.windowFrame = windowFrame;
        galleryPanel = this;

        setLayout(new BorderLayout());

        /*
        creates 'add' button
        adds actionListener to the button
        finally adds the button to a panel
         */
        JButton add = new JButton("add pictures");
        add.addActionListener(new ButtonListener());
        add.setActionCommand("add");
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(add);
        buttonsPanel.setBackground(Color.lightGray);

        /*
        creates 'change directory' button
        adds actionListener to the button
        finally adds the button to a panel
         */
        JButton change = new JButton("change directory");
        change.addActionListener(new ButtonListener());
        change.setActionCommand("change");
        buttonsPanel.add(change);


        /*
        pictures
         */

        //gallery instantiation
        gallery = new Gallery();


        //converts the files to imageIcons
        picturesImageIcons = loadImageIcons();

        //resizes pictures to fit in the disposition
        picturesImageIcons = Gallery.resizeAll(picturesImageIcons, PICTUREDIMENSION, PICTUREDIMENSION);

        /*
        adds every image to JLabels
        adds every JLabel to the center panel
         */
        addJLabelsToPanel();

        //adds action listener to every JLabel
        addPicturesMouseListener();

        /*
        converts the picturePanel to a scrollable component
        wrapLayout extends flowLayout and is used to enable the scroll function
         */
        picturesPanel.setLayout(new WrapLayout());
        JScrollPane scrollPane = new JScrollPane(picturesPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.getVerticalScrollBar().setUnitIncrement(8); //increase the scroll speed


        //initialises a JPanel with everything on it
        mainPanel = new JPanel();

        //content panel general settings
        mainPanel.setLayout(new BorderLayout());


        //adds the two sub-panels to the content panel
        mainPanel.add(buttonsPanel, BorderLayout.NORTH);
        mainPanel.add(scrollPane, BorderLayout.CENTER);

        add(mainPanel, BorderLayout.CENTER);

        setVisible(true);

        //will ask the user set a directory for the gallery
        if (gallery.isFirstLaunch()) {
            displaySetDirectoryPanel();
        }

    }

    /**
     * Converts the files from the gallery to ImageIcons in order to display them.
     *
     * @return ArrayList containing the ImageIcons from gallery
     */
    private ArrayList<ImageIcon> loadImageIcons() {

        ArrayList<File> picturesFile = gallery.getPictures();

        ArrayList<ImageIcon> imageIcons = new ArrayList<>(picturesFile.size());

        for (int i = 0; i < picturesFile.size(); i++) {
            imageIcons.add(new ImageIcon(gallery.getDirectoryPath() + picturesFile.get(i).getName()));
        }

        return imageIcons;
    }

    /**
     * Adds every image to JLabels.
     * Adds every JLabel to the center panel.
     */
    private void addJLabelsToPanel() {
        jLabels = new ArrayList<JLabel>(picturesImageIcons.size());
        picturesPanel = new JPanel();

        for (int i = 0; i < picturesImageIcons.size(); i++) {

            // adds every image to JLabels
            jLabels.add(new JLabel(picturesImageIcons.get(i)));

            //adds every JLabel to the center panel
            picturesPanel.add(jLabels.get(i));
        }
    }

    /**
     * Adds MouseListener to every JLabel containing a picture.
     */
    private void addPicturesMouseListener() {
        for (JLabel i : jLabels) {
            i.addMouseListener(new PictureMouseListener());
        }
    }


    /**
     * Adds the loaded picture to the screen.
     * Updates the JLabel and ImageIcon ArrayLists.
     * Adds a MouseListener to the new image.
     *
     * @param newImageFile to add
     */
    private void addPictureToFrame(File newImageFile) {

        int nbPictures = picturesImageIcons.size();

        String fileName = gallery.getDirectoryPath() + newImageFile.getName();

        //updates ImageIcons arrayList
        ImageIcon newImage = new ImageIcon(fileName);
        newImage = Gallery.resize(newImage, PICTUREDIMENSION, PICTUREDIMENSION);
        picturesImageIcons.add(newImage);
        nbPictures++;

        //updates JLabels array list
        jLabels.add(new JLabel(picturesImageIcons.get(nbPictures - 1)));
        jLabels.get(nbPictures - 1).addMouseListener(new PictureMouseListener());

        picturesPanel.add(jLabels.get(nbPictures - 1));

        //reduces the update delay on screen
        updateUI();

    }

    /**
     * Asks the parent Frame to reload a GalleryPanel in the new directory.
     * Writes the newDirectory in the txt file.
     *
     * @param directory new directory
     */
    protected void setDirectory(String directory) {
        gallery.changeDirectory(directory);
        launcherPanel.reloadGallery();
    }

    /**
     * Deletes the removed picture on screen.
     * Updates the JLabel and ImageIcons ArrayLists.
     * Removes the picture from the picture Panel.
     *
     * @param index of the deleted picture
     */
    protected void deletePictureOnScreen(int index) {
        picturesImageIcons.remove(index);
        jLabels.remove(index);
        picturesPanel.remove(index);
    }

    /**
     * Sets the GalleryPanel mainPanel invisible.
     * Displays the SetDirectoryPanel.
     */
    private void displaySetDirectoryPanel() {
        mainPanel.setVisible(false);

        setDirectoryPanel = new SetDirectoryPanel(galleryPanel, gallery);
        add(setDirectoryPanel, BorderLayout.CENTER);
        setDirectoryPanel.setVisible(true);
    }

    /**
     * Open the edition screen when a picture is clicked.
     */
    private class PictureMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {

            int indexOfImageClicked = jLabels.indexOf(e.getSource());
            gallery.setImageSelectedIndex(indexOfImageClicked);

            editionPanel = new EditionPanel(galleryPanel, gallery);

            editionPanel.setBounds(0, 0, WIDTHBOUND, HEIGHTBOUND);
            add(editionPanel);

            mainPanel.setVisible(false);
            editionPanel.setVisible(true);
        }
    }

    /**
     * Opens a file explorer when the add button is clicked.
     * Opens the SetDirectoryPanel when the change directory button is clicked.
     */

    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            switch (e.getActionCommand()) {
                case "add":
                    //displays the file explorer
                    FileDialog fileDialog = new FileDialog(windowFrame, "File explorer", FileDialog.LOAD);
                    fileDialog.setVisible(true);
                    try {

                        //doesn't throw exception when the user press "cancel"
                        if (fileDialog.getFile() != null) {

                            //gets the File which the user clicked
                            File clickedFile = new File(fileDialog.getDirectory() + fileDialog.getFile());

                            gallery.addPicture(clickedFile);

                            addPictureToFrame(clickedFile);

                        }

                    } catch (BusinessException exception) {
                        mainPanel.setVisible(false);

                        //displays the exception
                        ExceptionPanel exceptionPanel = new ExceptionPanel(mainPanel, exception.getMessage());
                        add(exceptionPanel, BorderLayout.CENTER);
                        exceptionPanel.setVisible(true);
                    }

                    break;

                case "change":

                    displaySetDirectoryPanel();

                    break;
            }
        }
    }


}

