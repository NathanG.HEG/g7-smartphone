package gallery;

import errors.BusinessException;
import genericFrames.LauncherPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 * <p>
 * RenamePanel is a JPanel which displays the current name of a given picture
 * and waits for the user to enter a new one.
 */

public class RenamePanel extends JPanel {

    //smartphone app dimension
    private final int HEIGHTBOUND = LauncherPanel.getHEIGHTBOUND();
    private final int WIDTHBOUND = LauncherPanel.getWIDTHBOUND();

    //graphic components
    private final EditionPanel editionPanel;
    private final JPanel contentPanel;
    private final JTextField newName;

    private final Gallery gallery;
    private int imageSelectedIndex;

    /**
     * @param editionPanel superPanel which is used to communicate between the Panels
     * @param gallery      same gallery as the one used in galleryPanel and editionPanel
     */

    public RenamePanel(EditionPanel editionPanel, Gallery gallery) {

        this.editionPanel = editionPanel;
        this.gallery = gallery;

        imageSelectedIndex = gallery.getImageSelectedIndex();

        //general settings
        setLayout(null);
        setBackground(Color.lightGray);

        //creates a Font for the renamePanel's text
        Font namesFont = new Font("SansSerif", Font.PLAIN, 18);

        //old name
        String oldName = "Current name : " + gallery.getPicture(imageSelectedIndex).getName();
        JLabel oldNameLabel = new JLabel(oldName);
        oldNameLabel.setFont(namesFont);

        //newName
        JLabel enterText = new JLabel("Enter new name : ");
        enterText.setFont(namesFont);

        //field waiting for the user input
        newName = new JTextField(22);

        //oldName is written in the textField for easier modification
        newName.setText(gallery.getPicture(imageSelectedIndex).getName());

        //adds the field and the 'Enter new name' text to a panel
        JPanel newNamePanel = new JPanel();
        newNamePanel.add(enterText);
        newNamePanel.add(newName);


        //adds both newName and oldName components to a Panel
        JPanel namesPanel = new JPanel();
        namesPanel.setLayout(new BorderLayout());
        namesPanel.add(oldNameLabel, BorderLayout.NORTH);
        namesPanel.add(newNamePanel, BorderLayout.CENTER);


        /*
        creates 'confirm' and 'back' buttons
        then adds actionListeners to each button
         */
        JButton confirm = new JButton("Confirm");
        confirm.setActionCommand("confirm");
        confirm.addActionListener(new ButtonListener());

        JButton back = new JButton("back");
        back.setActionCommand("back");
        back.addActionListener(new ButtonListener());

        //adds the two buttons to a dedicated panel
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(confirm);
        buttonsPanel.add(back);


        //creates a new JPanel with everything on it

        //content panel general settings
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBounds(30, 200, WIDTHBOUND - 90, HEIGHTBOUND - 450);

        //adds the two sub-panels to the content panel
        contentPanel.add(namesPanel, BorderLayout.CENTER);
        contentPanel.add(buttonsPanel, BorderLayout.SOUTH);

        add(contentPanel);

        setVisible(true);

    }

    /**
     * Case confirm :
     * tries to rename the picture
     * back to the edition screen if the new name is valid
     * displays exception screen if the new name is not valid
     * <p>
     * Case back :
     * back to the edition screen
     */
    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {

                //back to the edition screen
                case "back":
                    editionPanel.renamePanel.setVisible(false);
                    editionPanel.editionMainMenu.setVisible(true);
                    break;

                case "confirm":

                    try {

                        //picks up the user's input
                        String newNameString = newName.getText();

                        /*
                        tries to rename file on disk
                        throw an exception if the new name is not valid
                         */
                        String directory = gallery.getDirectoryPath();
                        File picture = new File(directory + gallery.getPicture(imageSelectedIndex).getName());
                        gallery.renameFile(picture, newNameString);

                        //updates the file name on the edition screen
                        editionPanel.updateFileName();

                        //back to edition screen
                        editionPanel.renamePanel.setVisible(false);
                        editionPanel.editionMainMenu.setVisible(true);

                    } catch (BusinessException exception) {

                        //resets the text field
                        newName.setText(gallery.getPicture(imageSelectedIndex).getName());

                        contentPanel.setVisible(false);

                        //displays the exception screen with the warning message
                        ExceptionPanel exceptionPanel = new ExceptionPanel(contentPanel, exception.getMessage());
                        exceptionPanel.setBounds(0, 0, WIDTHBOUND, HEIGHTBOUND);
                        add(exceptionPanel);
                        exceptionPanel.setVisible(true);
                    }

                    break;
            }
        }
    }
}
