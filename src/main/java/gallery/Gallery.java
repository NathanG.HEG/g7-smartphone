package gallery;

import errors.BusinessException;
import errors.ErrorCode;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 * <p>
 * A gallery is an object which contains an Arraylist of Files, a directory File
 * and an index of the selected image with getters and setters.
 * This allows the user to add, rename, delete and resizes png or jpg pictures.
 * Pictures can also be moved to a new directory.
 * The current directory is written on a txt file next to the jar file.
 */

public class Gallery {

    private final String galleryDirectoryOnDiskString;
    private final ArrayList<File> pictures = new ArrayList<File>();
    private File directory;
    private String directoryPath;
    private int imageSelectedIndex = -1;
    private boolean firstLaunch = false;

    /**
     * Constructor
     */
    public Gallery() {

        //places the txt next to the runnable file
        galleryDirectoryOnDiskString = "./galleryDirectory.txt";

        //checks if the user changed his directory during his last session
        String directoryFromTxt = readDirectory();

        /*
        checks if the gallery is launched for the first time
         */
        if (directoryFromTxt != null) {
            directoryPath = directoryFromTxt;

            this.directoryPath = checkDirectory(directoryPath);

            //source directory
            directory = new File(directoryPath);

            //initialises the gallery using the directory's existing content
            loadGallery();

        } else {
            firstLaunch = true;
        }
    }


    /**
     * Resizes an ImageIcon conserving the original width/height ratio.
     * Static modifier so it can be called without instancing a gallery.
     *
     * @param imageIcon to resize
     * @param width     max allowed width
     * @param height    max  allowed height
     * @return
     */
    public static ImageIcon resizeWithRatio(ImageIcon imageIcon, int width, int height) {

        double imageHeight = imageIcon.getIconHeight();
        double imageWidth = imageIcon.getIconWidth();

        double ratio = imageHeight / imageWidth;

        if (ratio < 1) {
            imageIcon = resize(imageIcon, width, (int) (height * ratio));
        } else {
            imageIcon = resize(imageIcon, ((int) (width * ratio)), height);
        }

        return imageIcon;
    }

    /**
     * Resizes an ImageIcon without conserving the original width/height ratio.
     * Might distort the picture if it has not square dimensions.
     * Static modifier so it can be called without instancing a gallery.
     *
     * @param imageIcon to resize
     * @param width     of the resized image
     * @param height    of the resized image
     * @return resized ImageIcon
     */
    public static ImageIcon resize(ImageIcon imageIcon, int width, int height) {

        // transforms it
        Image image = imageIcon.getImage();

        // scales it the smooth way
        Image newImg = image.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);

        // transforms it back
        imageIcon = new ImageIcon(newImg);

        return imageIcon;
    }

    /**
     * Resizes an ArrayList of ImageIcons without conserving the original width/height ratio.
     * Might distort the pictures if they have not square dimensions.
     * Uses gallery.resize method.
     * Static modifier so it can be called without instancing a gallery.
     *
     * @param imageIcons ArrayList of ImageIcon to resize
     * @param width      of the resized image
     * @param height     of the resized image
     * @return resized ImageIcon
     */
    public static ArrayList<ImageIcon> resizeAll(ArrayList<ImageIcon> imageIcons, int width, int height) {
        ArrayList<ImageIcon> newImageIcons = new ArrayList<>(imageIcons.size());

        for (int i = 0; i < imageIcons.size(); i++) {
            newImageIcons.add(resize(imageIcons.get(i), width, height));
        }

        return newImageIcons;
    }

    /**
     * Reads the txt on disk containing the last gallery's directory that the user set.
     * If the txt does not exist, it may be the first launch on a device.
     *
     * @return the gallery's directory
     */
    private String readDirectory() {

        File galleryDirectoryOnDisk = new File(galleryDirectoryOnDiskString);

        if (!galleryDirectoryOnDisk.exists()) {
            try {
                galleryDirectoryOnDisk.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        String galleryDirectoryFromTxt = null;

        try (BufferedReader in = new BufferedReader(new FileReader(galleryDirectoryOnDisk))) {
            galleryDirectoryFromTxt = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return galleryDirectoryFromTxt;

    }

    /**
     * Corrects the directory syntax if the user typed it wrong.
     * Adds a '/' at the end if the user forgot to.
     * Removes a '/' at the beginning if there is one.
     *
     * @param path check
     * @return checked path
     */
    public String checkDirectory(String path) {
        //adds a '/' at the end if the user forgot to
        if (!path.endsWith("/")) {
            path += "/";
        }

        //removes a '/' at the beginning if there is one
        if (path.startsWith("/")) {
            path = path.replaceFirst("/", "");
        }
        return path;
    }

    /**
     * Loads the gallery's directory content into an ArrayList of Files.
     * Only png or jpg files are loaded.
     * Does not throw any exception if the directory is empty.
     */

    private void loadGallery() {

        File[] directoryContent = directory.listFiles();

        //ensures to avoid nullPointerException
        if (directoryContent != null) {

            //only add the files having jpeg or png extension to the gallery
            for (File i : directoryContent) {
                if (checkFormat(i)) {
                    pictures.add(i);
                }
            }
        }
        /*
        gallery is empty
        no need to throw exception
        it will just display an empty gallery
         */
    }

    /**
     * Renames one picture at a time in the gallery ArrayList and files on disk.
     *
     * @param file    to rename
     * @param newName entered
     * @throws BusinessException if the name is empty, has the wrong extension, is identical,
     *                           is not unique in the gallery or if the File rename method failed
     */
    public void renameFile(File file, String newName) throws BusinessException {

        //saves the old path to update the arraylist later
        String oldName = directoryPath + file.getName();

        // File with new name
        File file2 = new File(directoryPath + newName);

        if (newName.isEmpty()) {
            throw new BusinessException("New name can't be empty", ErrorCode.EMPTY_NAME);
        }

        if (!checkFormat(file2)) {
            throw new BusinessException("File extension must be jpg, jpeg or png", ErrorCode.UNSUPPORTED_FORMAT);
        }

        if (file.getName().equals(newName)) {
            throw new BusinessException("New name must be different", ErrorCode.SIMILAR_NEW_NAME);
        }

        if (file2.exists()) {
            throw new BusinessException("File names must be unique", ErrorCode.ALREADY_EXISTING_FILE);
        }

        boolean success = file.renameTo(file2);

        //updates the arraylist
        if (success) {
            pictures.set(pictures.indexOf(new File(oldName)), file2);
        } else {
            throw new BusinessException("Invalid source or destination path", ErrorCode.INVALID_DIRECTORY);
        }


    }

    /**
     * Deletes one picture at a time in the gallery ArrayList and files on disk.
     *
     * @param file to delete
     * @throws BusinessException if the File remove method failed
     */
    public void deletePicture(File file) throws BusinessException {

        boolean success = file.delete();

        if (success) {
            pictures.remove(file);
        } else {
            throw new BusinessException("Technical error occurred while removing picture", ErrorCode.TECHNICAL_ERROR);
        }
    }

    /**
     * Adds one picture at a time in the gallery ArrayList and files on disk.
     * Only jpg or png files can be added.
     *
     * @param file to add
     * @throws BusinessException if the File argument has not a png or jpg extension, a File with an identical name already exists in the gallery,
     *                           an IOException occurs or File rename method failed
     */
    public void addPicture(File file) throws BusinessException {

        if (!checkFormat(file)) {
            throw new BusinessException("unsupported format", ErrorCode.UNSUPPORTED_FORMAT);
        }

        File file2 = new File(directoryPath + file.getName());

        //checks if the file already exists in the destination folder
        if (file2.exists()) {
            throw new BusinessException("Can't have two files named similarly in the gallery", ErrorCode.ALREADY_EXISTING_FILE);
        }

        //creates a copy to keep the file in the source directory
        File copyOfFile = new File(file.getParentFile().getAbsolutePath() + "/(1)" + file.getName());

        try {
            Files.copy(file.toPath(), copyOfFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new BusinessException("IOException thrown while copying the file", ErrorCode.IOEXCEPTION);
        }

        //moves the file on disk
        boolean success = copyOfFile.renameTo(file2);

        //updates the arrayList
        if (success) {
            pictures.add(file2);
        } else {
            throw new BusinessException("Technical error occurred while adding picture", ErrorCode.TECHNICAL_ERROR);
        }

    }

    /**
     * Checks the format of a given file.
     * Only allows files having jpg, png or jpeg extension.
     *
     * @param file
     * @return true if the file has a jpg, png or jpeg extension
     */
    private boolean checkFormat(File file) {

        String name = file.getName().toLowerCase();

        if (!name.endsWith(".jpg")) {
            if (!name.endsWith(".png")) {
                if (!name.endsWith(".jpeg")) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * A new Gallery must be created when changing the directory.
     * Writes the new directory in the txt File on disk.
     * Flushes the txt before writing in it.
     * When the new gallery is created, it'll use the written directory.
     *
     * @param newDirectory path where the picture are being moved
     */
    public void changeDirectory(String newDirectory) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(galleryDirectoryOnDiskString))) {
            bw.flush();
            bw.write(newDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public ArrayList<File> getPictures() {
        return pictures;
    }

    /**
     * Returns a picture at a given index.
     *
     * @param index of the picture
     * @return picture at the index
     */
    public File getPicture(int index) {
        return pictures.get(index);
    }

    /**
     * Returns a picture having the name given.
     *
     * @param fileName of the desired picture
     * @return corresponding picture
     */
    public File getPicture(String fileName) {
        return pictures.get(pictures.indexOf(new File(directoryPath + fileName)));
    }

    public boolean isFirstLaunch() {
        return firstLaunch;
    }

    public void setFirstLaunch(boolean value) {
        this.firstLaunch = value;
    }

    public int getImageSelectedIndex() {
        return imageSelectedIndex;
    }

    public void setImageSelectedIndex(int imageSelectedIndex) {
        this.imageSelectedIndex = imageSelectedIndex;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for (File i : pictures) {
            str.append(i.getAbsolutePath()).append("\n");
        }

        return str.toString();
    }

}
