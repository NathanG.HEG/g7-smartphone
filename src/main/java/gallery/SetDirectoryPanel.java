package gallery;

import errors.BusinessException;
import errors.ErrorCode;
import genericFrames.LauncherPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 * <p>
 * SetDirectoryPanel is a JPanel which displays the current gallery's directory
 * and waits for the user to enter a new one.
 */

public class SetDirectoryPanel extends JPanel {

    //smartphone app dimension
    private final int HEIGHTBOUND = LauncherPanel.getHEIGHTBOUND();
    private final int WIDTHBOUND = LauncherPanel.getWIDTHBOUND();

    //graphic components
    private final GalleryPanel galleryPanel;
    private final JPanel contentPanel;
    private final JTextField newDirectory;

    private Gallery gallery;

    /**
     * Constructor
     *
     * @param galleryPanel superPanel used to communicate between the Panels
     * @param gallery      gallery used in the superPanel
     */
    public SetDirectoryPanel(GalleryPanel galleryPanel, Gallery gallery) {

        this.galleryPanel = galleryPanel;
        this.gallery = gallery;

        //general settings
        setLayout(null);
        setBackground(Color.lightGray);

        //creates a Font for the setDirectoryPanel text
        Font directoriesFont = new Font("SansSerif", Font.PLAIN, 18);

        //old directory
        String oldDirectory = "Current directory : " + gallery.getDirectoryPath();
        JLabel oldNameLabel = new JLabel(oldDirectory);
        oldNameLabel.setFont(directoriesFont);

        //new directory
        JLabel enterText = new JLabel("Enter new directory : ");
        enterText.setFont(directoriesFont);

        //field waiting for the user input
        newDirectory = new JTextField(22);

        //adds the text field and the 'Enter new directory' text to a panel
        JPanel newDirectoryPanel = new JPanel();
        newDirectoryPanel.add(enterText);
        newDirectoryPanel.add(newDirectory);

        //adds both newDirectory and oldDirectory components to a Panel
        JPanel directoriesPanel = new JPanel();
        directoriesPanel.setLayout(new BorderLayout());
        directoriesPanel.add(oldNameLabel, BorderLayout.NORTH);
        directoriesPanel.add(newDirectoryPanel, BorderLayout.CENTER);

        /*
        creates 'confirm' and 'back' buttons
        then adds actionListeners to each button
         */
        JButton confirm = new JButton("Confirm");
        confirm.setActionCommand("confirm");
        confirm.addActionListener(new ButtonListener());

        JButton back = new JButton("back");
        back.setActionCommand("back");
        back.addActionListener(new ButtonListener());

        //adds the two buttons to a dedicated panel
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(confirm);
        buttonsPanel.add(back);


        //creates a new JPanel with everything on it

        //content panel general settings
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBounds(0, 200, WIDTHBOUND, HEIGHTBOUND - 450);

        //adds the two sub-panels to the content panel
        contentPanel.add(directoriesPanel, BorderLayout.CENTER);
        contentPanel.add(buttonsPanel, BorderLayout.SOUTH);

        add(contentPanel);

        setVisible(true);

    }

    /**
     * Check if a File exists and if it is a directory.
     *
     * @param path to check
     * @throws BusinessException if the file doesn't exist or if it isn't a directory
     */
    private void checkDirectoryExistence(String path) throws BusinessException {
        File file = new File(path);
        if (!file.exists()) {
            throw new BusinessException("This directory doesn't exist", ErrorCode.NON_EXISTING_DIRECTORY);
        }
        if (!file.isDirectory()) {
            throw new BusinessException("Only directories can contain pictures", ErrorCode.NOT_A_DIRECTORY);
        }
    }

    /**
     * Displays an exceptionPanel with the specified message.
     *
     * @param message to display
     */
    private void displayExceptionPanel(String message){

        ExceptionPanel exceptionPanel = new ExceptionPanel(contentPanel, message);
        exceptionPanel.setBounds(0, 0, WIDTHBOUND, HEIGHTBOUND);
        add(exceptionPanel);

        contentPanel.setVisible(false);
        exceptionPanel.setVisible(true);

    }


    /**
     * Case confirm :
     * changes directory and reloads gallery
     * <p>
     * Case back :
     * back to the galleryPanel screen
     */
    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {

                /*
                back to the gallery main screen
                cannot back when it is the first launch
                displays an exception screen instead
                 */
                case "back":

                    if(gallery.isFirstLaunch()){
                        displayExceptionPanel("A directory must be selected to use the gallery");
                    }else {
                        galleryPanel.setDirectoryPanel.setVisible(false);
                        galleryPanel.mainPanel.setVisible(true);
                    }
                    break;

                case "confirm":

                    //picks up the user's input
                    String newNameString = newDirectory.getText();

                    /*
                    adds a '/' at the end if the user forgot to
                    removes a '/' at the beginning if there is one
                     */
                    newNameString = gallery.checkDirectory(newNameString);

                    try {
                        checkDirectoryExistence(newNameString);
                    } catch (BusinessException exception) {
                        displayExceptionPanel(exception.getMessage());

                        //resets the entered text
                        newDirectory.setText("");
                        return;
                    }

                    //updates the directory
                    galleryPanel.setDirectory(newNameString);

                    //back to edition screen
                    galleryPanel.setDirectoryPanel.setVisible(false);
                    galleryPanel.mainPanel.setVisible(true);

                    break;

            }
        }


    }
}
