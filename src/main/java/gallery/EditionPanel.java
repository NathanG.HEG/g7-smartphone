package gallery;

import errors.BusinessException;
import genericFrames.LauncherPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 * <p>
 * EditionPanel is a JPanel which displays an image with its original width/height ratio.
 * From this screen, the user can delete the displayed picture.
 * Clicking rename opens the RenamePanel
 */

public class EditionPanel extends JPanel {

    //smartphone app dimension
    final int HEIGHTBOUND = LauncherPanel.getHEIGHTBOUND();
    final int WIDTHBOUND = LauncherPanel.getWIDTHBOUND();
    private final GalleryPanel galleryPanel;
    private final EditionPanel editionPanel;
    private final JLabel fileNameLabel;
    private final Gallery gallery;

    //graphic components
    JPanel editionMainMenu;
    JPanel renamePanel;
    private int imageIndex;

    /**
     * Constructor
     *
     * @param galleryPanel superPanel which is used to communicate between the Panels
     * @param gallery      same gallery as the one used in the superPanel
     */
    public EditionPanel(GalleryPanel galleryPanel, Gallery gallery) {

        this.galleryPanel = galleryPanel;
        this.editionPanel = this;

        //same gallery as the one in galleryFrame
        this.gallery = gallery;
        imageIndex = gallery.getImageSelectedIndex();

        //edition panel settings
        setLayout(null);
        setBackground(Color.lightGray);
        setBounds(0, 0, WIDTHBOUND, HEIGHTBOUND);


        //creates and resizes picture conserving the original width/height ratio
        ImageIcon clickedImage = new ImageIcon(gallery.getDirectoryPath() + gallery.getPicture(gallery.getImageSelectedIndex()).getName());
        clickedImage = Gallery.resizeWithRatio(clickedImage, 300, 300);
        JLabel imageLabel = new JLabel(clickedImage);


        //file name
        JTextField fileName = new JTextField();
        String fileNameString = gallery.getPicture(imageIndex).getName();
        fileName.setText(fileNameString);
        fileNameLabel = new JLabel(fileNameString);


        //picture and fileName panel
        JPanel picturesNamePanel = new JPanel(new BorderLayout());
        picturesNamePanel.add(imageLabel, BorderLayout.NORTH);
        picturesNamePanel.add(fileNameLabel, BorderLayout.CENTER);

        /*
        creates 'rename', 'delete' and 'back' buttons
        then adds actionListener to each button
         */

        //rename
        JButton rename = new JButton("rename");
        rename.setActionCommand("rename");
        rename.addActionListener(new ButtonListener());

        //delete
        JButton delete = new JButton("delete");
        delete.setActionCommand("delete");
        delete.addActionListener(new ButtonListener());

        //back
        JButton back = new JButton("back");
        back.setActionCommand("back");
        back.addActionListener(new ButtonListener());

        //creates a panel dedicated to the buttons
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(delete);
        buttonsPanel.add(rename);
        buttonsPanel.add(back);

        //initialises a JPanel with everything on it
        editionMainMenu = new JPanel();

        //content panel settings
        editionMainMenu.setBounds(30, 80, WIDTHBOUND - 90, HEIGHTBOUND - 250);
        editionMainMenu.setLayout(new BorderLayout());

        //adds the two sub-panels to the content panel
        editionMainMenu.add(picturesNamePanel, BorderLayout.NORTH);
        editionMainMenu.add(buttonsPanel, BorderLayout.SOUTH);

        add(editionMainMenu);

    }

    /**
     * Called when the picture's name is changed.
     * Updates the picture name on screen.
     */
    public void updateFileName() {
        String fileName = gallery.getPicture(imageIndex).getName();
        fileNameLabel.setText(fileName);
    }

    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "delete":
                    try {
                        gallery.deletePicture(gallery.getPicture(imageIndex));
                        galleryPanel.deletePictureOnScreen(imageIndex);


                        //back to gallery main screen if the picture was successfully deleted
                        galleryPanel.editionPanel.setVisible(false);
                        galleryPanel.mainPanel.setVisible(true);

                    } catch (BusinessException exception) {
                        editionMainMenu.setVisible(false);

                        //displays exception panel
                        ExceptionPanel exceptionPanel = new ExceptionPanel(editionMainMenu, exception.getMessage());
                        add(exceptionPanel, BorderLayout.CENTER);
                        exceptionPanel.setVisible(true);
                    }
                    break;


                case "rename":
                    //displays RenamePanel
                    editionMainMenu.setVisible(false);
                    renamePanel = new RenamePanel(editionPanel, gallery);
                    renamePanel.setBounds(0, 0, WIDTHBOUND, HEIGHTBOUND);
                    add(renamePanel);
                    renamePanel.setVisible(true);
                    break;


                //back to gallery main screen
                case "back":
                    galleryPanel.editionPanel.setVisible(false);
                    galleryPanel.mainPanel.setVisible(true);
                    break;
            }
        }
    }
}
