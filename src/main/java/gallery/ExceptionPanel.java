package gallery;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Benjamin Biollaz, benjamin.biollaz@students.hevs.ch
 * @since 10/06/2021
 * <p>
 * ExceptionPanel is a JPanel which displays the exception message with a picture.
 * ExceptionPanel's parent is a non-defined JPanel.
 * Therefore, any JPanel can create an ExceptionPanel.
 */

public class ExceptionPanel extends JPanel {

    private final JPanel superPanel;
    private final JPanel exceptionPanel;

    /**
     * Constructor
     *
     * @param superPanel       JPanel that created the ExceptionPanel
     * @param exceptionMessage Thrown exception's message
     */

    public ExceptionPanel(JPanel superPanel, String exceptionMessage) {

        this.superPanel = superPanel;
        exceptionPanel = this;

        //Exception panel general settings
        setBackground(Color.lightGray);
        setLayout(null);

        //ok button which closes the panel
        JButton ok = new JButton("ok");
        ok.addActionListener(new ButtonListener());

        //creates the warning image
        ImageIcon warningImage = null;
        try {
            warningImage = new ImageIcon(ImageIO.read(getClass().getClassLoader().getResource("warning_sign.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //resizes the warning image
        warningImage = Gallery.resizeWithRatio(warningImage, 100, 100);
        JLabel warningLabel = new JLabel(warningImage);

        //displays thrown exception's message
        JLabel textLabel = new JLabel(exceptionMessage);

        /*
        adds everything on the main panel
        addsthe main panel in the center
         */
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.add(warningLabel, BorderLayout.NORTH);
        contentPanel.add(textLabel, BorderLayout.CENTER);
        contentPanel.add(ok, BorderLayout.SOUTH);
        contentPanel.setBounds(45, 175, 300, 200);
        add(contentPanel);


    }

    /**
     * Closes the ExceptionPanel and goes back to superPanel
     */
    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            //back to the frame that threw the exception
            exceptionPanel.setVisible(false);
            superPanel.setVisible(true);
        }
    }
}
