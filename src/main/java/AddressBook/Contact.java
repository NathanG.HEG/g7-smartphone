package AddressBook;

import errors.BusinessException;
import errors.ErrorCode;

import java.util.ArrayList;

/**
 * The Contact Object with a minimum of a firstName attribute
 *
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class Contact {
    private String contactFirstName, contactLastName = "", notes;
    private ArrayList<ContactDetail> contactDetails = new ArrayList<ContactDetail>(2);
    private Birthday birthday;
    private boolean isBirthdayAddedToCalendar = false ;

    /**
     * Flag to tell is birthday is in the calendar
     *
     * @return true if already in calendar
     */
    public boolean isBirthdayAddedToCalendar() {
        return isBirthdayAddedToCalendar;
    }


    public void setBirthdayAddedToCalendar(boolean birthdayAddedToCalendar) {
        isBirthdayAddedToCalendar = birthdayAddedToCalendar;
    }

    /**
     * A first name is the only mandatory info to create a new contact
     *
     * @param firstName a string firstName required to build a contact
     */
    public Contact(String firstName) {
        contactFirstName = firstName;
    }

    public void addContactDetails(ContactDetail contactDetail) {
        contactDetails.add(contactDetail);
    }

    public Birthday getBirthday() {
        return birthday;
    }

    public void setBirthDay(int day, int month) throws BusinessException {
        if (month < 1 || month > 12 || day < 1 || day > 31) {
            throw new BusinessException("NOT A VALID DATE", ErrorCode.INVALID_DATE);
        }
        birthday = new Birthday(day,month);
    }

    public ArrayList<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        if (contactFirstName.equals("")) {
            contactFirstName = "unknown contact";
        }
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "AddressBook.Contact{" +
                "contactFirstName=" + contactFirstName + '\n' +
                "contactLastName=" + contactLastName + '\n' +
                "notes=" + notes + '\n' +
                "birthday=" + ((birthday != null) ? birthday.toString() : null) + '\n' +
                "contactDetails=" + contactDetails.toString() +
                '}' + '\n';
    }
}
