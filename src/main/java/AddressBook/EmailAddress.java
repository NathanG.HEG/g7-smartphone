package AddressBook;

import errors.BusinessException;
import errors.ErrorCode;

/**
 * ContactDetail of type EmailAddress
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class EmailAddress extends ContactDetail {
    /**
     * Constructor that checks if the email is valid using isEmailValid method
     * @param email string containing the email address
     * @throws Exception launched if the email is incorrect
     */
    public EmailAddress(String email) throws BusinessException {
        super(email);
        if(!isEmailValid(email)){
            throw new BusinessException("NOT A VALID EMAIL ADDRESS", ErrorCode.INVALID_EMAIL);
        }
    }

    /**
     * empty constructor used to deserialize Object
     */
    public EmailAddress(){
    }

    /**
     * Constructor that checks if the email is valid using isEmailValid method
     * @param email string containing the email address
     * @param description description Object (see Description Enum)
     * @throws Exception launched if the email is incorrect
     */
    public EmailAddress(String email, Description description) throws BusinessException {
        super(email, description);
        if(!isEmailValid(email)){
            throw new BusinessException("NOT A VALID EMAIL ADDRESS", ErrorCode.INVALID_EMAIL);
        }
    }

    @Override
    public String toString() {
        return "AddressBook.EmailAddress{" +
                "detail='" + detail + '\'' +
                ", description=" + description +
                '}';
    }

    /**
     * Verifies if the email address is correct
     * @param email the string that is set to be the new detail of EmailAddress
     * @return true if the email corresponds the regex
     */
    private boolean isEmailValid(String email){
        // It can only contains letters a-z, numbers, dashes, underscores or periods
        // But its prefix and domain can only start and finish with a letter a-z
        email.toLowerCase();
        String regex = "^[a-z][a-z0-9.-_]+[a-z]@[a-z][a-z0-9.-_]+[a-z]\\.[a-z]{2,3}$";
        return email.matches(regex);
    }

    @Override
    public void setDetail(String detail) throws Exception {
        if(!isEmailValid(detail)){
            throw new Exception("NOT A VALID EMAIL ADDRESS");
        }
        super.setDetail(detail);
    }
}
