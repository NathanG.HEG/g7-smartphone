package AddressBook;

import errors.BusinessException;
import errors.ErrorCode;

/**
 * A class that stores a Birthday with integers for month and day
 * Months ranges from 0 to 11
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class Birthday {
    private int day;

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    private int month;

    /**
     * Constructors that checks if the entered date exists
     * @param day the day in int format (1-31)
     * @param month the month in int format (0-11)
     * @throws BusinessException Throws an exception if the date cannot exist
     */
    public Birthday(int day, int month) throws BusinessException {
        if (month < 1 || month > 12 || day < 1 || day > 31) {
            throw new BusinessException("NOT A VALID DATE", ErrorCode.INVALID_DATE);
        }
        this.month = month;
        this.day = day;
    }

    /**
     * Constructor used in serialization
     */
    public Birthday(){}

    @Override
    public String toString() {
        return  day + "-" + (month);
    }
}
