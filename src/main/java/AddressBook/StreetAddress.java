package AddressBook;

import errors.BusinessException;
import errors.ErrorCode;

/**
 * ContactDetail of type StreetAddress
 *
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class StreetAddress extends ContactDetail {
    private int postCode;
    private String place;

    /**
     * empty constructor used to deserialize Object
     */
    public StreetAddress(){}

    /**
     * Constructor that checks if the phone number is valid using verifyPostCode and verifyPlace methods
     *
     * @param streetAddress string containing the streetAddress
     * @param place string containing the place
     * @param postCode int of the postal code
     * @throws Exception launched if the street address is incorrect
     */
    public StreetAddress(String streetAddress, String place, int postCode) throws BusinessException {
        super(streetAddress);
        this.place = place;
        this.postCode = postCode;
        if (!(verifyPlace(place) && verifyPostCode(postCode))) {
            throw new BusinessException("NOT A VALID ADDRESS", ErrorCode.INVALID_ADDRESS);
        }
    }

    /**
     * constructor used to deserialize Object
     */
    public StreetAddress(String detail) {
        super(detail);
    }
    /**
     * Constructor that checks if the phone number is valid using verifyPostCode and verifyPlace methods
     *
     * @param streetAddress string contaning the streetAddress
     * @param place string containing the place
     * @param postCode int of the postal code
     * @throws Exception launched if the street address is incorrect
     */
    public StreetAddress(String streetAddress, String place, int postCode, Description description) throws BusinessException {
        super(streetAddress, description);
        this.place = place;
        this.postCode = postCode;
        if (!(verifyPlace(place) || !verifyPostCode(postCode))) {
            throw new BusinessException("NOT A VALID ADDRESS", ErrorCode.INVALID_ADDRESS);
        }
    }

    @Override
    public String toString() {
        return "AddressBook.StreetAddress{" +
                "postCode=" + postCode +
                ", place='" + place + '\'' +
                ", description=" + description +
                '}';
    }

    /**
     * A postCode must be a strictly greater than 999 and lesser than 100000 positive integer
     * @param code int postal code
     * @return true if the postCode is correct
     */
    private boolean verifyPostCode(int code) {
        return !((code < 1000 || code > 99999));
    }

    /**
     * A place can only be written with letters a-z A-Z
     * @param place string containing the place
     * @return true if the place is correct
     */
    private boolean verifyPlace(String place) {
        String regex = "^[a-zA-Z]+$";
        return place.matches(regex);
    }
}
