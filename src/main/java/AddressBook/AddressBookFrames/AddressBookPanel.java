package AddressBook.AddressBookFrames;

import AddressBook.AddressBook;
import genericFrames.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * The main application of AddressBook
 *
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class AddressBookPanel extends ApplicationPanel {
    JPanel mainPanel = new JPanel();
    private AddressBookPanel addressBookFrame = this;
    private AddressBook ab = AddressBook.getAddressBook();
    private boolean isEditBtnAddedFlag = false;
    private JButton addBtn = new JButton("+");
    private JButton editBtn = new JButton("Contact info");
    private String selectedContactString = "contact";
    private JLabel selectedContactLbl = new JLabel("Selected contact: no selected contact");
    private JList<String> contactJList;
    private JPanel topPanel = new JPanel();
    private EditContactPanel editContactFrame;
    private AddContactPanel addContactFrame;
    private JLabel emptyContactListLabel = new JLabel("no contact");

    /**
     * Default constructor, places the panel correctly in the main panel of Launcher
     */
    public AddressBookPanel() {
        ab.loadContacts(ab.getJarPath()+"/contacts.json");
        setLayout(null);
        add(mainPanel);
        mainPanel.setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
        mainPanel.setLayout(new BorderLayout());
        if (ab.getContacts().size() != 0) {
            contactJList = new JList<String>(contactsNameList(ab));
            contactJList.addListSelectionListener(new JListActionListener());
            mainPanel.add(contactJList, BorderLayout.CENTER);
        } else {
            mainPanel.add(emptyContactListLabel, BorderLayout.CENTER);
        }
        mainPanel.add(topPanel, BorderLayout.NORTH);
        addBtn.addActionListener(new ButtonListener());
        editBtn.addActionListener(new ButtonListener());
        addBtn.setActionCommand("+");
        editBtn.setActionCommand("Contact info");
        topPanel.add(selectedContactLbl);
        topPanel.add(addBtn);

    }

    /**
     * sets the contactJList
     *
     * @param contactJList
     * @deprecated
     */
    public void setContactJList(JList<String> contactJList) {
        this.contactJList = contactJList;
    }

    /**
     * Updates the JList when a contact has been modified or added
     */
    public void updateJList() {
        if (ab.getContacts().size() != 0) {
            if (contactJList != null) {
                mainPanel.remove(contactJList);
            } else {
                mainPanel.remove(emptyContactListLabel);
            }
            contactJList = new JList<String>(contactsNameList(ab));
            contactJList.addListSelectionListener(new JListActionListener());
            contactJList.setSelectedIndex(0);
            mainPanel.add(contactJList, BorderLayout.CENTER);
        } else {
            selectedContactLbl.setText("");
            if (contactJList != null) {
                mainPanel.remove(contactJList);
            }
            mainPanel.add(emptyContactListLabel, BorderLayout.CENTER);
        }
    }

    /**
     * Creates a DefaultListModel with the firstName and lastName attributes of all contacts
     *
     * @param ab AddressBook (can create a new AddressBook() as it uses a Singleton Pattern
     * @return DefaultListModel used to create a Jlist
     */
    private DefaultListModel<String> contactsNameList(AddressBook ab) {
        DefaultListModel<String> contactsNameList = new DefaultListModel<>();
        for (int i = 0; i < ab.getContacts().size(); i++) {
            contactsNameList.addElement(ab.getContacts().get(i).getContactFirstName() + " " +
                    ab.getContacts().get(i).getContactLastName());
        }
        return contactsNameList;
    }

    public class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "Contact info":
                    if (ab.getContacts().size() != 0) {
                        mainPanel.setVisible(false);
                        editContactFrame = new EditContactPanel(addressBookFrame);
                        editContactFrame.setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
                        add(editContactFrame);
                    }
                    break;
                case "+":
                    mainPanel.setVisible(false);
                    addContactFrame = new AddContactPanel(addressBookFrame);
                    addContactFrame.setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
                    add(addContactFrame);
                    break;
            }
        }
    }

    public class JListActionListener implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!isEditBtnAddedFlag) {
                topPanel.add(editBtn);
                isEditBtnAddedFlag = true;
            }
            if (contactJList.getSelectedIndex() != -1) {
                selectedContactLbl.setText("Selected contact: " + contactJList.getSelectedValue());
                ab.setSelectedContact(contactJList.getSelectedIndex());
                //System.out.println(ab.getSelectedContact());
            } else {
                selectedContactString = "Selected contact: no selected contact";
            }
        }
    }

}
