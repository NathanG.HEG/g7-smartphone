package AddressBook.AddressBookFrames;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * An exception panel that indicates to the user what went wrong
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class NewDetailExceptionPanel extends JPanel {
    private AddDetailFrame superPanel;
    private JPanel contentPanel;
    private JPanel exceptionPanel;
    private JButton ok;

    /**
     * Default constructor
     * @param superPanel the panel that called the ExceptionPanel
     * @param exceptionMessage the message of the error the user caused
     */
    public NewDetailExceptionPanel(AddDetailFrame superPanel, String exceptionMessage) {
        this.superPanel = superPanel;
        exceptionPanel = this;

        //setBackground(Color.red);
        setBackground(Color.LIGHT_GRAY);
        setLayout(null);

        ok = new JButton("ok");
        ok.addActionListener(new ButtonListener());

        JLabel textLabel = new JLabel(exceptionMessage);
        System.out.println("AddressBook.AddressBookFrames.ExceptionPanel created");
        System.out.println(exceptionMessage);
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.add(textLabel, BorderLayout.CENTER);
        contentPanel.add(ok, BorderLayout.SOUTH);
        contentPanel.setBounds(45, 175, 300, 200);
        contentPanel.setBackground(Color.GRAY);
        contentPanel.setVisible(true);
        add(contentPanel);
        setVisible(true);
    }

    class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            exceptionPanel.setVisible(false);
            contentPanel.setVisible(false);
            ok.setVisible(false);
            superPanel.setLayout(new FlowLayout());
            superPanel.fieldPanel.setVisible(true);
        }
    }
}
