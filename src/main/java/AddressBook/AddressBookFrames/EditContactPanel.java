package AddressBook.AddressBookFrames;

import AddressBook.*;

import genericFrames.ApplicationPanel;
import genericFrames.LauncherPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * A frame to edit the attributes of a Contact Object or add new ContactDetails
 * @author Nathan Gailard
 * @since 2021-06-10
 */
public class EditContactPanel extends ApplicationPanel {
    private final static int SIZEOFFIELD = 23;

    AddressBook ab = AddressBook.getAddressBook();
    private AddressBookPanel addressBookFrame;
    private EditContactPanel editContactFrame;
    private JButton backBtn = new JButton("<");
    private JPanel infoPanel = new JPanel();
    private JButton newDetailBtn = new JButton("+ detail");
    private JButton deleteBtn = new JButton("delete contact");
    private JLabel selectedContactLabel;
    private JTextField firstNameField = new JTextField(SIZEOFFIELD);
    private JTextField lastNameField = new JTextField(SIZEOFFIELD);
    private JTextField notesField = new JTextField(SIZEOFFIELD);
    private JTextField birthdayField = new JTextField(SIZEOFFIELD);
    private JLabel firstNameLabel = new JLabel("First name: ");
    private JLabel lastNameLabel = new JLabel("Last name: ");
    private JLabel notesLabel = new JLabel("Notes: ");
    private JLabel birthdayLabel = new JLabel("Birthday: ");
    JScrollPane infoPanelJScrollPane;
    JPanel topComponentsPanel = new JPanel();
    private Contact currentContact;
    private JLabel contactDetailsLabel[];
    private JTextField contactDetailsField[];

    /**
     * Default constructor used to extend AddContactPanel
     */
    public EditContactPanel(){}

    /**
     * Constructor used in AddressBookPanel to edit the selected contact
     * @param addressBookFrame takes the frame that invoked AddressBookPanel to return to it later
     */
    public EditContactPanel(AddressBookPanel addressBookFrame) {
        // misc
        setLayout(null);
        setBackground(Color.LIGHT_GRAY);
        setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
        editContactFrame = this;
        this.addressBookFrame = addressBookFrame;
        if (ab.getContacts().size() > 0) {
            selectedContactLabel = new JLabel(ab.getContacts().get(ab.getSelectedContact()).getContactFirstName()
                    + " " + ab.getContacts().get(ab.getSelectedContact()).getContactLastName());
            currentContact = ab.getContacts().get(ab.getSelectedContact());
            contactDetailsLabel = new JLabel[currentContact.getContactDetails().size()];
            contactDetailsField = new JTextField[currentContact.getContactDetails().size()];
        }
        //top components
        topComponentsPanel.setBounds(10, 20, LauncherPanel.getWIDTHBOUND() - 40, 30);
        topComponentsPanel.setBackground(this.getBackground());

        deleteBtn.addActionListener(new ButtonListener());
        deleteBtn.setActionCommand("delete contact");

        backBtn.addActionListener(new ButtonListener());
        backBtn.setActionCommand("<");

        newDetailBtn.addActionListener(new ButtonListener());
        newDetailBtn.setActionCommand("+Detail");

        topComponentsPanel.add(backBtn);
        topComponentsPanel.add(selectedContactLabel);
        topComponentsPanel.add(newDetailBtn);
        topComponentsPanel.add(deleteBtn);
        topComponentsPanel.setVisible(true);
        add(topComponentsPanel);

        /* Builds the entire info panel*/
        infoPanel.setBackground(Color.GRAY.brighter());
        infoPanel.setLayout(new SpringLayout());

        firstNameField.setText(currentContact.getContactFirstName());
        firstNameLabel.setLabelFor(firstNameField);
        infoPanel.add(firstNameLabel);
        infoPanel.add(firstNameField);

        lastNameField.setText(currentContact.getContactLastName());
        lastNameLabel.setLabelFor(lastNameField);
        infoPanel.add(lastNameLabel);
        infoPanel.add(lastNameField);

        notesField.setText(currentContact.getNotes());
        notesLabel.setLabelFor(notesField);
        infoPanel.add(notesLabel);
        infoPanel.add(notesField);

        if (currentContact.getBirthday() != null) {
            birthdayField.setText(currentContact.getBirthday().toString());
        }
        birthdayLabel.setLabelFor(birthdayField);
        infoPanel.add(birthdayLabel);
        infoPanel.add(birthdayField);

        buildFields();

        infoPanelJScrollPane = buildInfoPanelJScrollPane(infoPanel);
        add(infoPanelJScrollPane);

        setVisible(true);
    }

    /**
     * Builds the scrollable panel with all the selected contact attributes
     * @param infoPanel the panel that contains all the selected contact attributes
     * @return the Scrollable panel to add to the frame
     */
    private JScrollPane buildInfoPanelJScrollPane(JPanel infoPanel) {
        SpringUtilities.makeCompactGrid(infoPanel, 4 + currentContact.getContactDetails().size(), 2, 6, 6, 6, 6);
        //ScrollBar
        JPanel scrollablePanel = new JPanel();
        scrollablePanel.setLayout(new WrapLayout());
        scrollablePanel.add(infoPanel);
        infoPanelJScrollPane = new JScrollPane(scrollablePanel,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        infoPanelJScrollPane.getVerticalScrollBar().setUnitIncrement(8);
        infoPanelJScrollPane.setBounds(0, 70, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
        return infoPanelJScrollPane;
    }

    /**
     * Updates the panel containing all contact attributes when one attribute is added
     * @param contactDetail the contactDetail that was created in NewDetailPanel
     */
    public void updateScrollableInfoPanel(ContactDetail contactDetail) {
        JLabel newContactLabel = new JLabel(contactDetail.getClass().getTypeName() + ": ");
        JTextField newContactField = new JTextField(SIZEOFFIELD);
        newContactField.setText(contactDetail.getDescription() + " " + contactDetail.getDetail());
        newContactLabel.setLabelFor(newContactField);
        infoPanel.add(newContactLabel);
        infoPanel.add(newContactField);
        infoPanelJScrollPane = buildInfoPanelJScrollPane(infoPanel);
        remove(infoPanelJScrollPane);
        add(infoPanelJScrollPane);
    }

    /**
     * Builds all the fields of contactDetails according to their size and type
     */
    private void buildFields() {
        for (int i = 0; i < currentContact.getContactDetails().size(); i++) {
            contactDetailsLabel[i] = new JLabel(currentContact.getContactDetails().get(i).getClass().getSimpleName() + ": ");
            contactDetailsField[i] = new JTextField(SIZEOFFIELD);
            contactDetailsField[i].setText(currentContact.getContactDetails().get(i).getDescription()
                    + " " + currentContact.getContactDetails().get(i).getDetail());
            contactDetailsLabel[i].setLabelFor(contactDetailsField[i]);
            infoPanel.add(contactDetailsLabel[i]);
            infoPanel.add(contactDetailsField[i]);
        }
    }

    /**
     * Updates contact details when a modification is finished
     * @return the success of the modification
     */
    public boolean updateContact() {
        currentContact.setContactFirstName(firstNameField.getText());
        currentContact.setContactLastName(lastNameField.getText());
        currentContact.setNotes(notesField.getText());
        if (birthdayField.getText() != null && !birthdayField.getText().equals("")) {
            try {
                currentContact.setBirthDay(Integer.parseInt(birthdayField.getText().split("-")[0]),
                        Integer.parseInt(birthdayField.getText().split("-")[1]));
                System.out.println(birthdayField.getText().split("-")[0] + " " + birthdayField.getText().split("-")[1]);
            } catch (Exception e) {
                //System.out.println(e.getMessage());
                ExceptionPanel exceptionPanel = new ExceptionPanel(editContactFrame, e.getMessage());
                exceptionPanel.setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
                infoPanelJScrollPane.setVisible(false);
                topComponentsPanel.setVisible(false);
                add(exceptionPanel);
                exceptionPanel.setVisible(true);
                return false;
            }
        }
        // makes sure the text field starts with HOME, WORK or SCHOOL or naught
        // assures the details are correct
        for (int i = 0; i < contactDetailsField.length; i++) {
            try {
                if (contactDetailsField[i].getText().split(" ", 2).length >= 2) {
                    currentContact.getContactDetails().get(i).setDetail(contactDetailsField[i].getText().split(" ", 2)[1]);
                    switch (contactDetailsField[i].getText().split(" ", 2)[0]) {
                        case "WORK":
                            currentContact.getContactDetails().get(i).setDescription(Description.WORK);
                            break;
                        case "SCHOOL":
                            currentContact.getContactDetails().get(i).setDescription(Description.SCHOOL);
                            break;
                        default:
                            currentContact.getContactDetails().get(i).setDescription(Description.HOME);
                            break;
                    }
                } else {
                    currentContact.getContactDetails().get(i).setDetail(contactDetailsField[i].getText());
                    currentContact.getContactDetails().get(i).setDescription(Description.HOME);
                }
            } catch (Exception e) {
                //System.out.println(e.getMessage());
                //String message = contactDetailsLabel[i] + " " + e.getMessage();
                ExceptionPanel exceptionPanel = new ExceptionPanel(editContactFrame, e.getMessage());
                exceptionPanel.setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
                infoPanelJScrollPane.setVisible(false);
                topComponentsPanel.setVisible(false);
                add(exceptionPanel);
                exceptionPanel.setVisible(true);
                return false;
            }
        }
        return true;
    }
    /**
     * Updates the JList in AddressBookPanel and goes back to the panel that called EditContactPanel
     * only occurs if the update has been successful
     */
    private void back() {
        if (updateContact()) {
            editContactFrame.setVisible(false);
            addressBookFrame.updateJList();
            addressBookFrame.mainPanel.setVisible(true);
        }
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "<":
                    back();
                    break;
                case "delete contact":
                    ab.deleteContact(ab.getSelectedContact());
                    back();
                    break;
                case "+Detail":
                    infoPanelJScrollPane.setVisible(false);
                    topComponentsPanel.setVisible(false);
                    AddDetailFrame addDetailFrame = new AddDetailFrame(editContactFrame);
                    addDetailFrame.setBounds(0, 0, LauncherPanel.getWIDTHBOUND(), LauncherPanel.getHEIGHTBOUND());
                    addDetailFrame.setVisible(true);
                    add(addDetailFrame);
            }
        }
    }
}


