package AddressBook.AddressBookFrames;

import AddressBook.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A frame to add a new contact, only adds a contact first name
 * Made to be extensible to add as many details of contact as needed in the future
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class AddContactPanel extends EditContactPanel {
    private final static int SIZEOFFIELD = 23;
    private AddContactPanel addContactFrame = this;
    private AddressBook ab = AddressBook.getAddressBook();
    private AddressBookPanel addressBookFrame;
    private JLabel newNameLabel;
    private JTextField newNameField;
    private JButton save = new JButton("Save new contact");
    private JButton cancel = new JButton("Cancel");

    /**
     * Default constructor
     * @param addressBookFrame is the frame that called AddContactPanel. Used to return to it.
     */
    public AddContactPanel(AddressBookPanel addressBookFrame) {
        this.addressBookFrame = addressBookFrame;
        newNameLabel = new JLabel("Enter new contact first name: ");
        newNameField = new JTextField("new contact");
        newNameLabel.setLabelFor(newNameField);
        newNameField.setColumns(SIZEOFFIELD);
        add(newNameLabel);
        add(newNameField);
        save.addActionListener(new ButtonListener());
        cancel.addActionListener(new ButtonListener());
        save.setActionCommand("SAVE");
        cancel.setActionCommand("CANCEL");
        add(cancel);
        add(save);
        addContactFrame.setVisible(true);
    }

    /**
     * Sets the screen to the frame that called AddContactPanel
     */
    private void back() {
        addContactFrame.setVisible(false);
        addressBookFrame.updateJList();
        addressBookFrame.mainPanel.setVisible(true);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "SAVE":
                    if (!newNameField.getText().equals(""))
                        ab.addContact(new Contact(newNameField.getText()));
                    back();
                    break;
                case "CANCEL":
                    back();
                    break;
            }
        }
    }
}
