package AddressBook.AddressBookFrames;

import AddressBook.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A JPanel used to add a specific details to a contact
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class AddDetailFrame extends JPanel {
    private final static int SIZEOFFIELD = 23;
    private AddDetailFrame addDetailFrame = this;
    private EditContactPanel superPanel;
    JPanel fieldPanel = new JPanel();
    private JTextField detailField = new JTextField(SIZEOFFIELD);
    private JButton ok = new JButton("OK");
    private Contact currentContact;
    private ContactDetail addedDetail;
    private JLabel detailLabel = new JLabel();
    private JButton buttons[] = new JButton[3];

    /**
     * Default constructor
     * @param editContactFrame is the frame that called AddDetailFrame. Used to return to it.
     */
    public AddDetailFrame(EditContactPanel editContactFrame) {
        superPanel = editContactFrame;
        setBackground(Color.LIGHT_GRAY);
        currentContact = superPanel.ab.getContacts().get(superPanel.ab.getSelectedContact());
        buttons[0] = new JButton("New email");
        buttons[1] = new JButton("New street address");
        buttons[2] = new JButton("New phone number");
        buttons[0].setActionCommand("ADDEMAIL");
        buttons[2].setActionCommand("ADDPHONENUMBER");
        buttons[1].setActionCommand("ADDSTREETADDRESS");
        for(int i=0; i<buttons.length; i++){
            buttons[i].addActionListener(new ButtonListener());
            add(buttons[i]);
        }
    }

    /**
     * Adds an EmailAddress Object to the contactDetails array in the selected contact
     */
    private void addEmailAddress() {
        ok.setActionCommand("EMAIL");
        ok.addActionListener(new ButtonListener());
        detailLabel.setText("Email :");
        detailLabel.setLabelFor(detailField);
        for(int i = 0; i<buttons.length; i++)
            buttons[i].setVisible(false);
        fieldPanel.add(detailLabel);
        fieldPanel.add(detailField);
        fieldPanel.add(ok);
        add(fieldPanel);
    }

    /**
     * Adds an PhoneNumber Object to the contactDetails array in the selected contact
     */
    private void addPhoneNumber() {
        ok.setActionCommand("PHONENUMBER");
        ok.addActionListener(new ButtonListener());
        detailLabel.setText("Phone number :");
        detailLabel.setLabelFor(detailField);
        for(int i = 0; i<buttons.length; i++)
            buttons[i].setVisible(false);
        fieldPanel.add(detailLabel);
        fieldPanel.add(detailField);
        fieldPanel.add(ok);
        add(fieldPanel);
    }

    /**
     * Adds an StreetAddress Object to the contactDetails array in the selected contact
     */
    private void addStreetAddress() {
        ok.setActionCommand("STREETADDRESS");
        ok.addActionListener(new ButtonListener());
        detailLabel.setText("Address :");
        detailLabel.setLabelFor(detailField);
        for(int i = 0; i<buttons.length; i++)
            buttons[i].setVisible(false);
        fieldPanel.add(detailLabel);
        fieldPanel.add(detailField);
        fieldPanel.add(ok);
        add(fieldPanel);
    }

    /**
     * Returns to the panel that called AddDetailFrame
     */
    private void back() {
        superPanel.updateScrollableInfoPanel(addedDetail);
        setVisible(false);
        superPanel.infoPanelJScrollPane.setVisible(true);
        superPanel.topComponentsPanel.setVisible(true);
    }

    class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "ADDEMAIL":
                    addEmailAddress();
                    break;
                case "ADDPHONENUMBER":
                    addPhoneNumber();
                    break;
                case "ADDSTREETADDRESS":
                    addStreetAddress();
                    break;
                case "EMAIL":
                    try {
                        if (detailField.getText().split(" ", 2).length >= 2) {
                            addedDetail = new EmailAddress(detailField.getText().split(" ", 2)[1]);
                            switch (detailField.getText().split(" ", 2)[0]) {
                                case "WORK":
                                    addedDetail.setDescription(Description.WORK);
                                    break;
                                case "SCHOOL":
                                    addedDetail.setDescription(Description.SCHOOL);
                                    break;
                                default:
                                    addedDetail.setDescription(Description.HOME);
                                    break;
                            }
                        } else {
                            addedDetail = new EmailAddress(detailField.getText());
                            addedDetail.setDescription(Description.HOME);
                        }
                        currentContact.addContactDetails(addedDetail);
                        back();
                    } catch (Exception exception) {
                        runException(exception);
                    }
                    break;
                case "STREETADDRESS":
                    try {
                        if (detailField.getText().split(" ", 2).length >= 2) {
                            addedDetail = new StreetAddress(detailField.getText().split(" ", 2)[1]);
                            switch (detailField.getText().split(" ", 2)[0]) {
                                case "WORK":
                                    addedDetail.setDescription(Description.WORK);
                                    break;
                                case "SCHOOL":
                                    addedDetail.setDescription(Description.SCHOOL);
                                    break;
                                default:
                                    addedDetail.setDetail(detailField.getText());
                                    addedDetail.setDescription(Description.HOME);
                                    break;
                            }
                        } else {
                            addedDetail = new StreetAddress(detailField.getText());
                            addedDetail.setDescription(Description.HOME);
                        }
                        currentContact.addContactDetails(addedDetail);
                        back();
                    } catch (Exception exception) {
                        runException(exception);
                    }
                    break;
                case "PHONENUMBER":
                    try {
                        if (detailField.getText().split(" ", 2).length >= 2) {
                            addedDetail = new PhoneNumber(detailField.getText().split(" ", 2)[1]);
                            switch (detailField.getText().split(" ", 2)[0]) {
                                case "WORK":
                                    addedDetail.setDescription(Description.WORK);
                                    break;
                                case "SCHOOL":
                                    addedDetail.setDescription(Description.SCHOOL);
                                    break;
                                default:
                                    addedDetail.setDescription(Description.HOME);
                                    break;
                            }
                        } else {
                            addedDetail = new PhoneNumber(detailField.getText());
                            addedDetail.setDescription(Description.HOME);
                        }
                        currentContact.addContactDetails(addedDetail);
                        back();
                    } catch (Exception exception) {
                        runException(exception);
                    }
                    break;
            }
        }
    }

    /**
     * Creates an exception panel to inform the user that something went wrong
     * @param e Exception that occured
     */
    private void runException(Exception e){
        NewDetailExceptionPanel exceptionPanel = new NewDetailExceptionPanel(addDetailFrame, e.getMessage());
        fieldPanel.setVisible(false);
        setLayout(new BorderLayout());
        add(exceptionPanel, BorderLayout.CENTER);
        exceptionPanel.setVisible(true);
    }
}
