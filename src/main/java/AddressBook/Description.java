package AddressBook;

/**
 * The enum that specifies a ContactDetail
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public enum Description {
    HOME, WORK, SCHOOL;
}
