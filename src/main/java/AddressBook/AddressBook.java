package AddressBook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Address book class containing the contacts and the save/load methods
 *
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class AddressBook {
    private static AddressBook addressBook = new AddressBook();
    private final String jarName = "G7-Smartphone-jar-with-dependencies.jar";
    private ArrayList<Contact> contacts = new ArrayList<>();
    private int selectedContact;

    /**
     * private constructor used in the singleton pattern
     */
    private AddressBook() {
    }

    /**
     * method used to get the unique addressBook in the program
     *
     * @return shared AddressBook object (see Singleton Pattern)
     */
    public static AddressBook getAddressBook() {
        return addressBook;
    }

    /**
     * delete a contact at index i
     *
     * @param i index of the contact to delete in the contacts ArrayList
     */
    public void deleteContact(int i) {
        contacts.remove(i);
    }

    /**
     * Get the contacts ArrayList of the address book
     *
     * @return ArrayList<Contact>
     */
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    /**
     * add the contact in parameter to the address book ArrayList
     *
     * @param newContact contact that will be added to the ArrayList
     */
    public void addContact(Contact newContact) {
        contacts.add(newContact);
    }

    /**
     * Returns the contact previously selected
     *
     * @return int a the index of the selected contact
     */
    public int getSelectedContact() {
        return selectedContact;
    }

    /**
     * Sets the index indicating of the selected Contact
     *
     * @param selectedContact index of the contact being selected
     */
    public void setSelectedContact(int selectedContact) {
        this.selectedContact = selectedContact;
    }

    /**
     * Returns the path where the running jar file is located
     *
     * @return a path where the running jar file is located
     */
    public String getJarPath() {
        String jarPath = AddressBook.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = null;
        try {
            decodedPath = URLDecoder.decode(jarPath, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        decodedPath = decodedPath.replaceFirst("/", "");
        decodedPath = decodedPath.replaceAll(jarName, "");
        return decodedPath;
    }

    /**
     * Adds the contacts in the json file specified in @param path using gson deserialization
     * If the file is corrupted : terminates the program
     * If the file is empty : creates a new empty ArrayList<Contacts> in AddressBook
     *
     * @param path path of the json file containing the contacts
     */
    public void loadContacts(String path) {
        try {
            File contactsFile = new File(path);
            if (!contactsFile.exists()) {
                contactsFile.createNewFile();
            }
            Gson gson = new Gson();
            Type contactType = new TypeToken<ArrayList<Contact>>() {
            }.getType();
            FileReader fr = new FileReader(contactsFile);
            BufferedReader br = new BufferedReader(fr);
            contacts = gson.fromJson(br, contactType);
            if (contacts == null) {
                contacts = new ArrayList<Contact>();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Saves the contacts in the json file specified next to the JAR file using gson serialization in JAR environment
     */
    public void saveContactsInJarEnvir() {
        File contactsFile;
        try {
            contactsFile = new File(getJarPath() + "/contacts.json");
            /*RuntimeTypeAdapterFactory<ContactDetail> adapter =
                    RuntimeTypeAdapterFactory
                            .of(ContactDetail.class)
                            .registerSubtype(PhoneNumber.class)
                            .registerSubtype(EmailAddress.class)
                            .registerSubtype(StreetAddress.class);*/
            Gson gson = new Gson/*Builder().setPrettyPrinting().registerTypeAdapterFactory(adapter).create*/();
            String json = gson.toJson(contacts);
            FileWriter myWriter = new FileWriter(contactsFile);
            myWriter.write(json);
            myWriter.close();
            contactsFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Saves the contacts in the json file specified in @param path using gson serialization
     * If json file is absent : creates a new json file in specified @param path
     *
     * @param path path of the json file containing the contacts
     */
    public void saveContacts(String path) {
        File contactsFile = new File(path);

        try {
            contactsFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RuntimeTypeAdapterFactory<ContactDetail> adapter =
                RuntimeTypeAdapterFactory
                        .of(ContactDetail.class)
                        .registerSubtype(PhoneNumber.class)
                        .registerSubtype(EmailAddress.class)
                        .registerSubtype(StreetAddress.class);
        Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(adapter).create();
        ;
        String json = gson.toJson(contacts);
        try {
            FileWriter myWriter = new FileWriter(contactsFile);
            myWriter.write(json);
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method only used for debugging purpose
     * Shows all contacts and their details in console
     */
    public void showContentInConsole() {
        for (Contact c : getContacts()) {
            System.out.println(c.toString());
        }
    }
}
