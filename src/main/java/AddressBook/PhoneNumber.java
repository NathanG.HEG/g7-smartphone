package AddressBook;

import errors.BusinessException;
import errors.ErrorCode;

/**
 * ContactDetail of type PhoneNumber
 *
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public class PhoneNumber extends ContactDetail {
    /**
     * Constructor that checks if the phone number is valid using isPhoneNumberValid method
     *
     * @param phoneNumber string containing the phone number
     * @throws Exception launched if the phone number is incorrect
     */
    public PhoneNumber(String phoneNumber) throws BusinessException {

        super(phoneNumber);
        if (!isPhoneNumberValid(phoneNumber)) {
            throw new BusinessException("NOT A VALID PHONE NUMBER", ErrorCode.INVALID_PHONENUMBER);
        }
    }

    /**
     * empty constructor used to deserialize Object
     */
    public PhoneNumber() {
    }

    /**
     * Constructor that checks if the phone number is valid using isPhoneNumberValid method
     *
     * @param phoneNumber string containing the phone number
     * @param description description Object (see Description Enum)
     * @throws Exception launched if the phone number is incorrect
     */
    public PhoneNumber(String phoneNumber, Description description) throws BusinessException {
        super(phoneNumber, description);
        if (!isPhoneNumberValid(phoneNumber)) {
            throw new BusinessException("NOT A VALID PHONE NUMBER", ErrorCode.INVALID_PHONENUMBER);
        }
    }

    @Override
    public String toString() {
        return "AddressBook.PhoneNumber{" +
                "detail='" + detail + '\'' +
                ", description=" + description +
                '}';
    }

    /**
     * A phone number must start with a '0' and be shorter than 25 char.
     * OR start with a '+41' and be 12 char. long
     * only swiss numbers are verified and can be entered with the +XX format
     * @param phoneNumber the phone number that is set to be the new detail of PhoneNumber
     * @return true if the phone number is valid
     */
    private boolean isPhoneNumberValid(String phoneNumber) {
        if (phoneNumber.startsWith("+41")) {
            return (phoneNumber.length() == 12);
        }
        return (phoneNumber.charAt(0) == '0' && phoneNumber.length() < 26 && phoneNumber.matches("^[0-9]+$"));
    }

    @Override
    public void setDetail(String detail) throws Exception {
        if (!isPhoneNumberValid(detail)) {
            throw new Exception("NOT A VALID PHONE NUMBER");
        }
        super.setDetail(detail);
    }
}
