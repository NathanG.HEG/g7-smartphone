package AddressBook;

/**
 * ContactDetail class
 * Supposed to be an abstract class, blocks the serialization if it is.
 * @author Nathan Gaillard
 * @since 2021-06-10
 */
public /*abstract*/ class ContactDetail {
    String detail;
    Description description;

    /**
     * Creates an Contact detail. Default description is HOME Only used in deserialization
     * @param detail the string containing the detail
     */
    public ContactDetail(String detail) {
        this.detail = detail;
        description = Description.HOME;
    }

    /**
     * Default constructor used in deserialization
     */
    public ContactDetail(){}

    /**
     * Creates an Contact detail specifying the Description.
     * @param detail the string containing the detail
     * @param description description Object (see Description Enum)
     */
    public ContactDetail(String detail, Description description) {
        this.detail = detail;
        this.description = description;
    }

    @Override
    public String toString() {
        return  "detail='" + detail + '\'' +
                ", description=" + description +
                '}';
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) throws Exception {
        this.detail = detail;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) throws Exception {
        this.description = description;
    }
}
